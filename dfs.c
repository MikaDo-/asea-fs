#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "hardware.h"
#include "drive.h"
#include "mount.h"
#include "vol.h"
#include "mbr.h"

extern mbr_str MBR;
extern unsigned int secSize;

int main(int argc, char **argv)
{
	printf("\nUtilitaire dfs \"Display FileSystems\" - ASE TP 8\n=================================================\n						Alexis Beaujet M1\n\n\n");
    
	initConfig();
	startDisk(); 
 
    printf("\n  Volume name  |   Size   |   Used   |   Dispo   |  Utilise  \n");
    int currentVol;
    for (currentVol = 0; currentVol < MBR.volCount; currentVol++){
        int err = 0;
        if( (err = load_superblock(currentVol))){
            fprintf(stderr, "ERREUR - dfs : Impossible de charger le volume %d : err %d\n", currentVol, err);
            continue;
        }
        unsigned int freespace = volFreeSpace() * secSize;
        unsigned int total = MBR.volumes[currentVol].size * secSize;
        printf("  %8s     |  %5uB  |  %5uB  |   %5uB  |   %2.2f %%  \n", vol_name(), total, total-freespace, freespace, 100*(total-freespace)/(float)total);
    }
    printf("\n");
	exit(EXIT_SUCCESS);
}
