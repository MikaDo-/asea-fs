#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "hardware.h"
#include "sched.h"
#include "boot.h"
#include "disk.h"
#include "drive.h"
#include "vol.h"
#include "mbr.h"
#include "mount.h"

extern mbr_str MBR;
extern unsigned int secSize;

unsigned int nbSec, nbCyl;

/* ------------------------------
   command list
   ------------------------------------------------------------*/
struct _cmd {
    char *name;
    void (*fun) (struct _cmd *c);
    char *comment;
};

static void list(struct _cmd *c);	// OK
static void wipe(struct _cmd *c);	// OK
static void new(struct _cmd *c);	// OK
static void del(struct _cmd *c);	// OK
static void help(struct _cmd *c) ;	// OK
static void save(struct _cmd *c);	// OK
static void quit(struct _cmd *c);	// OK
static void xit(struct _cmd *c);
static void none(struct _cmd *c);

static struct _cmd commands [] = {
    {"list", list, 	"display the partition table"},
    {"new",  new,	"create a new partition"},
    {"del",  del,	"delete a partition"},
    {"save", save,	"save the MBR"},
    {"quit", quit,	"save the MBR and quit"},
    {"exit", xit,	"exit (without saving)"},
    {"help", help,	"display this help"},
    {"wipe", wipe,	"creates a new partition table"},
    {0,      none, 		"unknown command, try help"}
} ;

/* ------------------------------
   dialog and execute 
   ------------------------------------------------------------*/

static void
execute(const char *name)
{
    struct _cmd *c = commands; 
  
    while (c->name && strcmp (name, c->name))
	c++;
    (*c->fun)(c);
}

static void
loop()
{
    char name[64];
    while (printf("> "), scanf("%62s", name) == 1)
	execute(name) ;
}

/* ------------------------------
   command execution 
   ------------------------------------------------------------*/
static void
list(struct _cmd *c)
{
	CHECK_MBR_OR_DIE
    // Afficher la table des partitions...
	printf("Table actuelle des partitions :\n");
	printf(" | # | pos(cyl,sec) | size (B) | type |\n");
	#define currentVol MBR.volumes[i]
	for(int i = 0;i<MBR.volCount;i++)
		printf(" | %1d |  (%2d, %3d )  | %7dB |   %1d  |\n", i, currentVol.cyl, currentVol.sec, currentVol.size*secSize, currentVol.type);
}

static void
wipe(struct _cmd *c)
{
    // Pour effacer la table, on met juste le nombre de partitions à zéro.
	MBR.volCount = 0;
	MBR.magic = MBR_MAGIC;
	printf("INFO  - vm : MBR reinistialise !\n");
}

static void
new(struct _cmd *c)
{
	CHECK_MBR_OR_DIE
    // créer une nouvelle partition
	if(MBR.volCount >= 8){
		printf("Nombre maximal de partition atteint.\n");
		return;
	}
	printf("    =========================\n    == CREER UNE PARTITION ==\n    =========================\n\n");

	unsigned int pos;
	unsigned int offset;
	unsigned int size;
	unsigned int partType;
	do{
		printf("    1. Id de la partition existante qui precedera celle en train detre cree : (10 pour en tête)\n        ");
		fflush(stdin);
		scanf(" %u", &pos);
	}while(pos >= MBR.volCount && !(pos == 10 || (pos <= 0 && pos >= 8)));
	
	printf("    2. Decallage en octets depuis la fin de la partition precedente : (ou depuis le debut du disque si en tete)\n        ");
	scanf("%u", &offset);
	fflush(stdin);
	printf("    3. Taille en octets de la nouvelle partition :\n    ");
	scanf("%u", &size);
	do{
		printf("    4. Type de la nouvelle partition :  ( 1. systeme de fichiers - 2. XXX - 3. YYY )\n    ");
		scanf("%u", &partType);
	}while(partType > 3 || partType < 1);
	
	offset = ceil((double)offset/(double)secSize);
	// ! \ On ne veut pas qu'une partition en tête englobe le premier secteur.
	// ! \ Il faut donc ajouter systématiquement un décallage de 1 secteur;
	if(pos == 10)
		offset++;
	size = ceil((double)size/(double)secSize);
	// on remet tout en secteurs (en arrondissant au dessus)
	if(pos != 10)
		offset += MBR.volumes[pos].size + MBR.volumes[pos].sec + MBR.volumes[pos].cyl * nbSec;
	// on a la position de la future partition. Vérifions s'il y a la place pour la loger.

	// Si la partition est plus grande que le disque ou placent une partie de la partition en dehors du disque.
	if(size + offset >= nbCyl*nbSec || offset >= nbCyl*nbSec){
		fprintf(stderr, "ERROR - vm : Les informations saisies placent au moins une partie de la partition en dehors du disque.\n");
		return;
	}
	// on génère un nouveau MBR à partir de l'ancien et des infos reçues.
	mbr_str newMBR = MBR;
	if(pos != (MBR.volCount-1) && // si on se place après la dernière partition, 
	   MBR.volCount >  0){    	  // aucun autre test n'est nécessaire.
								  // Par contre si on se place entre deux, il faut vérifier que la fin de la
								  // nouvelle partition ne dépassera pas le début de la suivante.
		unsigned int offsetNextPart;
		if(pos != 10)
			offsetNextPart = MBR.volumes[pos+1].cyl * nbSec + MBR.volumes[pos+1].sec;
		else
			offsetNextPart = MBR.volumes[0].cyl * nbSec + MBR.volumes[0].sec;
		if(size + offset >= offsetNextPart){
			fprintf(stderr, "ERROR - vm : Les informations saisies font se superposer la nouvelle partition et la suivante.\n");
			return;
		}
		for(int i = pos+1;i<newMBR.volCount;i++)		// on décalle les partitions devant pour préparer l'insertion de la nouvelle.
			newMBR.volumes[i+1] = newMBR.volumes[i];
	}
	if(pos == 10)
		pos = 0;
	else
		pos++;
	newMBR.volumes[pos].cyl  = offset / nbSec;
	newMBR.volumes[pos].sec  = offset % nbSec;
	newMBR.volumes[pos].size = size;
	newMBR.volumes[pos].type = partType;
	newMBR.volCount++;
	MBR = newMBR;
}

static void
del(struct _cmd *c)
{
	CHECK_MBR_OR_DIE
	printf("Supprimer une partition : \n");
	if(MBR.volCount <= 0){
		printf("INFO  - vm : Aucune partition a supprimer.\n");
		return;
	}
	int partNum;
	do{
		printf("\tSaisissez le numero de la partition a supprimer :\n");
		scanf("%d", &partNum);
	}while(partNum >= MBR.volCount || partNum < 0);
	for(int i = partNum;i<MBR.volCount-1;i++)
		MBR.volumes[i] = MBR.volumes[i+1];
	MBR.volCount--;
}

static void
save(struct _cmd *c)
{
	CHECK_MBR_OR_DIE
	mbr_write(MBR);
    printf("INFO  - vm : MBR correctement sauvegarde.\n");
}

static void
quit(struct _cmd *c)
{
    // Quitter mais faire un truc avant... 
	printf("Quitter :\n");
	printf("\tVoulez-vous sauvez les modifications non-enregistrees ? (y/n) ");
	char ans;
	scanf("%c", &ans);
	if(ans == 'y')
		mbr_write(MBR);
    printf("\n");
	exit(EXIT_SUCCESS);
}

static void
do_xit()
{
    printf("\n");
    exit(EXIT_SUCCESS);
}

static void
xit(struct _cmd *dummy)
{
    do_xit(); 
}

static void
help(struct _cmd *dummy)
{
    struct _cmd *c = commands;
  
    for (; c->name; c++) 
	printf ("%s\t-- %s\n", c->name, c->comment);
}

static void
none(struct _cmd *c)
{
    printf ("%s\n", c->comment) ;
}

int
main(int argc, char **argv)
{
	printf("\nEdition de la table des partitions - ASE TP 7\n=============================================\n						Alexis Beaujet M1\n\n\n");
	
	int err = 0;
	if( (err = initConfig()) )
		return err;
	if( (err = startDisk()) )
		;//return err;
    loop();

    /* dialog with user */ 
    /* abnormal end of dialog (cause EOF for xample) */
    do_xit();

    /* make gcc -W happy */
    exit(EXIT_SUCCESS);
}
