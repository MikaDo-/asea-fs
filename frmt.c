#include "stdio.h"
#include "stdlib.h"
#include "stdint.h"

#include "hardware.h"
#include "drive.h"

extern unsigned int secSize;

// On format le disque entier.
void frmt(unsigned int val){
   	unsigned int nbcyl, nbsec, taillesec;
	nbcyl = nbsec = taillesec = 0;
	getGeometry(&nbcyl, &nbsec, &taillesec);
    for(int i = 0;i<nbcyl;i++)
		for(int j = 0;j<nbsec;j++){
			mSeek(i, j);
	        mFormat(i, j, 1, val);
		}
}

int main(int argc, char* argv[]){
	
	printf("Outil de formatage frmt - ASE TP 6\n==================================\n						Alexis Beaujet M1\n\n\n");
    if(init_hardware("hardware.ini") == 0) {
	    fprintf(stderr, "ERROR - frmt : Error in hardware initialization\n");
    	exit(EXIT_FAILURE);
    }
	initDisk();
	
	uint16_t formatVal;
	printf("argc : %d\n", argc);
	if(argc == 2)
		formatVal = atoi(argv[1]);
	else
		formatVal = 0x0;

	printf("INFO  - frmt : Debut du formatage...\n");
	frmt(formatVal);
    printf("INFO  - frmt : Disque maitre formate avec la valeur 0x%08x.\n\n", formatVal);

    return EXIT_SUCCESS;
}
