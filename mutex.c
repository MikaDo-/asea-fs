#include <stdlib.h>
#include "sched.h"
#include "semaphore.h"
#include "mutex.h"

extern ctx_s* ctx_current;


void mtx_init(Mutex* m){
	sem_init(&(m->sem), 1);
}
void mtx_lock(Mutex* m){
	if(m->owner && m->owner == ctx_current) // au cas où un ctx lock plusieurs fois.
		return;
	sem_down(&(m->sem));
	m->owner = ctx_current;
}
void mtx_unlock(Mutex* m){
	if(ctx_current != m->owner)
		return;
	sem_up(&(m->sem));
	m->owner = NULL;
}
