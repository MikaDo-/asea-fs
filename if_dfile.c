#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "hardware.h"
#include "drive.h"
#include "vol.h"
#include "mbr.h"
#include "inode.h"
#include "ifile.h"
#include "mount.h"

static int dfile(unsigned int inumber){
    int err;
    if( (err = delete_ifile(inumber)) )
    fprintf(stderr, "ERROR - dfile : ERROR WHILE DELETING FILE %u. - err: %d\n", inumber, err);
        return err;
}

static void usage(const char *prgm){
    fprintf(stderr, "[%s] usage:\n\t"
            "%s inumber\n", prgm, prgm);
    exit(EXIT_FAILURE);
}

int main (int argc, char *argv[]){
    unsigned inumber;
    
    if (argc != 2)
        usage(argv[0]);

    errno = 0;
    inumber = strtol(argv[1], NULL, 10);
    if (errno || inumber <= 0)
        usage(argv[0]);

    mount();
    dfile(inumber);
    umount();
    
    exit(EXIT_SUCCESS);         
}
