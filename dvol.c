#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hardware.h"
#include "drive.h"
#include "vol.h"
#include "mbr.h"

extern mbr_str MBR;
extern unsigned int secSize;

void list(){// Afficher la table des partitions...
    
	CHECK_MBR_OR_DIE
	printf("Table actuelle des partitions :\n");
	printf(" | # | pos(cyl,sec) | size (B) | type |\n");
	#define currentVol MBR.volumes[i]
	for(int i = 0;i<MBR.volCount;i++)
		printf(" | %1d |  (%2d, %3d )  | %7dB |   %1d  |\n", i, currentVol.cyl, currentVol.sec, currentVol.size*secSize, currentVol.type);
}

int main(int argc, char **argv)
{
	printf("\nEdition de la table des partitions - ASE TP 7\n=============================================\n						Alexis Beaujet M1\n\n\n");
    if(init_hardware("hardware.ini") == 0) {
	    fprintf(stderr, "ERREUR - dvol : Error in hardware initialization\n");
    	exit(EXIT_FAILURE);
    }
	initDisk(); // se charge d'initialiser l'IRQ vector, récupérer taille de secteur...
	printf("INFO  - dvol : Chargement de la table des partitions...");
	mbr_load();
	if(checkMBR() >=0 && MBR.volCount <= 8)
		printf("    [OK]\n");
	else{
		printf("    [ECHEC]\n");
		fprintf(stderr, "ERREUR - dvol : La table des partitions est illisible ou non initialisee. Il est necessaire d'en recreer une nouvelle.\n\n");
	}
    list();
    exit(EXIT_SUCCESS);
}
