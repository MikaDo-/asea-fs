#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "signal.h"
#include "hardware.h"
#include "debug_tools.h"
#include "mount.h"
#include "drive.h"
#include "vol.h"
#include "mbr.h"

extern mbr_str MBR;
extern unsigned int secSize;
extern unsigned int currentVol;

int main(int argc, char **argv)
{
	printf("\nInitalisation d'un systeme de fichiers dans $CURRENT_VOLUME - ASE TP 8\n======================================================================\n						Alexis Beaujet M1\n\n\n");
    int err = 0;
    initConfig();
	if( (err = startDisk()) )
		return err;

    if( (err = makefs(currentVol)) )
        fprintf(stderr, "ERROR - mkfs : Impossible d'initaliser un systeme de fichiers. (err %d)\n", err);
    else
        printf("INFO  - mkfs : Systeme de fichiers correctement initialise.\n");
	return err;
}
