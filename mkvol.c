#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"
#include "getopt.h"
#include "string.h"
#include "ctype.h"
#include "hardware.h"
#include "drive.h"
#include "vol.h"
#include "mbr.h"

extern mbr_str MBR;
extern unsigned int secSize;

unsigned int nbSec, nbCyl;

// taille exprimée en secteurs.
int mkvol(unsigned int cyl, unsigned int sec, unsigned int size, unsigned int partType){
    CHECK_MBR_OR_DIE_I
	if(MBR.volCount >= 8){
		fprintf(stderr, "ERROR - mkvol : Nombre maximal de partition atteint.\n");
		return -7;
	}
    
    unsigned int offset = cyl*nbSec + sec; // avec nbSec = nombre de secteurs par cylindre.
    if(offset == 0) // on en veut pas remplacer le MBR
        return -8;

	// Si la partition est plus grande que le disque ou place une partie de la partition en dehors du disque.
	if(size + offset >= nbCyl*nbSec || offset >= nbCyl*nbSec){
		fprintf(stderr, "ERROR - mkvol : Les informations saisies placent une partie ou la totalite de la partition en dehors du disque.\n");
		return -9;
	}
    // On détermine la position de la nouvelle partition dans le MBR.
    int pos = 0;
    unsigned int newPartInf = offset;
    unsigned int newPartSup = offset + size;
    unsigned int currentPartInf, currentPartSup;
    for (int i = 0; i < MBR.volCount; i++){
        // pour simplifier les comparaisons, on remet tout en secteurs.
        currentPartInf = MBR.volumes[i].cyl * nbSec + MBR.volumes[i].sec;
        currentPartSup = currentPartInf + MBR.volumes[i].size;
        if(newPartInf > currentPartSup && newPartSup > currentPartSup) // newPart se trouve après currentPartInf
            pos++;
        else if (! (newPartInf < currentPartInf && newPartSup < currentPartInf) ){ // sinon, si ça ne se trouve pas avant non plus, il y a superposition !
            fprintf(stderr, "ERROR - mkvol : Les coordonnees saisies font se superposer deux volumes.\n");
            fprintf(stderr, "ERROR - mkvol : Collision avec volume %d (cyl,sect,size)=(%u,%u,%u)!\n", i, MBR.volumes[i].cyl, MBR.volumes[i].sec, MBR.volumes[i].size);
            fprintf(stderr, "ERROR - mkvol : Preferez utiliser le gestionnaire de volume ./vm .\n");
            return -10;
        }
    }
    printf("INFO  - mkvol : Le volume sera cree en position %d.\n", pos);
        
	// on génère un nouveau MBR à partir de l'ancien et des infos reçues.
	mbr_str newMBR = MBR;
    for(int i = pos+1;i<newMBR.volCount;i++)		// on décalle les partitions devant pour préparer l'insertion de la nouvelle.
        newMBR.volumes[i] = newMBR.volumes[i-1];
	newMBR.volumes[pos].cyl  = offset / nbSec;
	newMBR.volumes[pos].sec  = offset % nbSec;
	newMBR.volumes[pos].size = size;
	newMBR.volumes[pos].type = partType;
	newMBR.volCount++;
	MBR = newMBR;
    mbr_write();
    
    return 0;
}

int main(int argc, char **argv)
{
	printf("\nUtilitaire de creation de volumes - ASE TP 8\n============================================\n						Alexis Beaujet M1\n\n\n");
    
    char *hwConfig=NULL;
    
    hwConfig = getenv("HW_CONFIG");
    if(!hwConfig){
        hwConfig = (char*)malloc(255);
        sprintf(hwConfig, "%s", "hardware.ini");
    }
    printf("INFO  - mkvol : hwpath = %s\n", hwConfig);
    
    if(init_hardware(hwConfig) == 0) {
	    fprintf(stderr, "ERROR - mkvol : Hardware initialization failed.\n");
    	exit(EXIT_FAILURE);
    }
	initDisk(); // se charge d'initialiser l'IRQ vector, récupérer taille de secteur...
	getGeometry(&nbCyl, &nbSec, &secSize);
	printf("INFO  - mkvol : Chargement de la table des partitions...");
	mbr_load();
	if(checkMBR() >=0 && MBR.volCount <= 8)
		printf("    [OK]\n");
	else{
		printf("    [ECHEC]\n");
		fprintf(stderr, "ERROR - mkvol : La table des partitions est illisible ou non initialisee.\n");
        fprintf(stderr, "ERROR - mkvol : Il est necessaire d'en recreer une nouvelle. Utilisez la commande 'wipe' dans l'utilitaire vm.\n");
        return -11;
	}
    
    /*
     * On parse les arguments passés.
     * -s  : taille volume
     * -fc : cylindre de départ
     * -fs : secteur de départ
     */
    unsigned int cyl, sec, size;
    int cylDefined = 0, secDefined = 0, sizeDefined = 0;
    int c, fActif = 0; // sert à distinguer -s de -fs, magouille moche mais inévitable avec getopt...
    while ((c = getopt (argc, argv, "fs:c:")) != -1)
        switch (c){
            case 'c':
                cyl = atoi(optarg);
                if(cyl > nbCyl){
                    fprintf (stderr, "ERROR - mkvol : Option -%c doit etre suivi d'un numero de cylindre.\n", optopt);
                    return -12;
                }
                cylDefined = 1;
                fActif = 0;
                break;
            case 'f':
                fActif = 1; // le prochain -s sera à considérer comme -fs, -c comme -fc
                break;
            case 's':
                if(fActif){ // -fs
                    fActif = 0;
                    sec = atoi(optarg);
                    secDefined = 1;
                }else{
                    size = atoi(optarg);
                    sizeDefined = 1;
                }
                break;
            case '?':
                if (optopt == 'c')
                    fprintf (stderr, "ERROR - mkvol : L'option -%c doit etre suivi d'un numero de cylindre.\n", optopt);
                else if (optopt == 's' && fActif)
                    fprintf (stderr, "ERROR - mkvol : L'option -fs doit etre suivi d'un numero de secteur.\n");
                else if (optopt == 's' && !fActif)
                    fprintf (stderr, "ERROR - mkvol : L'option -s doit etre suivi d'une taille en secteurs.\n");
                else if (isprint (optopt))
                    fprintf (stderr, "ERROR - mkvol : Option inconnue `-%c'.\n", optopt);
                else
                    fprintf (stderr, "ERROR - mkvol : Caractere inconnu `\\x%x'.\n", optopt);
                return 1;
            default:
                abort ();
        }
    printf("INFO  - mkvol : mkvol(%u, %u, %u, 1)\n", cyl, sec, size);
    if(!sizeDefined){
        fprintf (stderr, "ERROR - mkvol : L'option -s doit etre precisee.\n");
        return -14;
    }if(!secDefined){
        fprintf (stderr, "ERROR - mkvol : L'option -fs doit etre precisee.\n");
        return -15;
    }if(!cylDefined){
        fprintf (stderr, "ERROR - mkvol : L'option -fc doit etre precisee.\n");
        return -16;
    }
    
    int err = 0;
    if( (err = mkvol(cyl, sec, size, 1)) )
        fprintf(stderr, "ERROR - mkvol : %d\n\n", err);
    else
        printf("INFO  - mkvol : Volume cree avec succes.\n");
	return err;
}
