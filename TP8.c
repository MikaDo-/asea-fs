#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "hardware.h"
#include "drive.h"
#include "vol.h"
#include "mbr.h"
#include "inode.h"
#include "mount.h"

extern mbr_str MBR;
extern unsigned int secSize;
extern superblock_str currentSuperblock;

extern unsigned int currentVol;

void testsAllocationBlocks();
void testsVblocks();

/*
 * Test des couches 2 & 3
 */

int main(int argc, char **argv)
{
	printf("\nTest de l'allocation/liberation des blocs - ASE TP 8\n====================================================\n						Alexis Beaujet M1\n\n\n");
    
    mount();
    makefs(currentVol);

    char choix[200];
    do{
        printf("\n\nMenu : (EOF pour sortir)\n\n   1- Tests d'allocation/libération de blocs (gestion de la freelist et du superblock) : SUR PETIT VOLUME SINON LENT\n   2- Tests d'allocation de datablocks et de tables d'indirection au sein d'un inode\n\n");
        fflush(stdin);
        if(scanf("%s", choix) == EOF)
            exit(0);
    }while(strlen(choix) != 1 || atoi(choix) > 2 || atoi(choix) < 1);
    
    switch (atoi(choix)){
        case 1:
            testsAllocationBlocks();
            break;
        case 2:
            testAllocationDataBlocks();
            break;
        default:
            printf("Loule.\n");
    }
    
	printf("\n");
	exit(EXIT_SUCCESS);
}

void testsVblocks(){
    initTestIndx();
    dump_superblock();
    printf("print_inode(2) : %d\n", print_inode(1));
    unsigned int testBlock = vblock_of_fblock(1, 100, false);
    printf("vblock_of_fblock(1, 100) = %u\n", testBlock);
    printf("Volume %s plein\n", (isVolFull() == 0) ? "NON ":"");
    printf("Espace restant : %d/%d (1 bloc reserve au superbloc)\n", volFreeSpace(), MBR.volumes[currentVol].size);
    disp_freelist();
}


void testsAllocationBlocks(){
    
    printf("Volume %s plein\n", (isVolFull() == 0) ? "NON ":"");
    printf("Espace restant : %d/%d (1 bloc reserve au superbloc)\n", volFreeSpace(), MBR.volumes[currentVol].size);
    
    disp_freelist();
    
    int newBlock;
    while( (newBlock = new_block()) > 0){
        printf("Allocation du bloc %04d.\n", newBlock); // pour un volume de 10 240o, on pourra allouer 39 blocs (1 bloc réservé pour superblock) 40*256o=10240o
        printf("Espace restant : %d\n\n", volFreeSpace());
    }
    printf("Allocation impossible : err %d\n", newBlock);
    printf("Espace restant : %d/%d\n", volFreeSpace(), MBR.volumes[currentVol].size);
    
    printf("Volume %splein.\n", (isVolFull() == 0) ? "NON ":"");
    
    printf("Liberation de le moitie des blocs du volume.\n");
    unsigned int i;
    for (i = 1; i < MBR.volumes[currentVol].size/2; i++) // Libération de blocs contingüs.
        free_block(i);
        
    disp_freelist();
        
    // ré-allocation de 2 blocs
    for(unsigned int i = 0;i<2 && (newBlock = new_block()) > 0;i++){
        printf("Allocation du bloc %04d.\n", newBlock);
        printf("Espace restant : %d\n\n", volFreeSpace());
    }
    
    // libération de blocs au hasard
    unsigned int blockToFree[] = { 10, 15 };
    unsigned int blockToFreeCount = 2;
    for (i = 0; i < blockToFreeCount; i++){
        printf("Liberation du bloc n %d - statut : %d\n", blockToFree[i], free_block(blockToFree[i]));
        printf("Permier bloc libre : %u\n", currentSuperblock.firstFree);
        printf("Espace restant : %d/%d\n", volFreeSpace(), MBR.volumes[currentVol].size);
        printf("Volume %s plein\n", (isVolFull() == 0) ? "NON ":"");
    }
    
    disp_freelist();
    
    // ré-allocation de 2 blocs
    for(unsigned int i = 0;i<2 && (newBlock = new_block()) > 0;i++){
        printf("Allocation du bloc %04d.\n", newBlock);
        printf("Espace restant : %d\n\n", volFreeSpace());
    }
    
    // affichage freelist
    disp_freelist();
    
    printf("Permier bloc libre : %u\n", currentSuperblock.firstFree);
    printf("Espace restant : %d/%d\n", volFreeSpace(), MBR.volumes[currentVol].size);
    printf("Volume %s plein\n", (isVolFull() == 0) ? "NON ":"");
}


