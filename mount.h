#ifndef MOUNT_H_INCLUDED
#define MOUNT_H_INCLUDED

int mount();
int initConfig();
int startDisk();
void umount();

#endif

