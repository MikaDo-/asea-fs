#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "debug_tools.h"
#include "sched.h"
#include "boot.h"
#include "timer.h"
#include "sched.h"
#include "disk.h"

extern ctx_s* ctx_current; // contexte actuel
extern ctx_s* roundRobin;  // boucle des contextes actifs

int i = 0;

void f_testDisk(void *args){
	// on test le disque en lui envoyant des jobs.
	unsigned char buff[512];
	_dprint("DEBUT DE testDisk\n");
	disk_read(0, 1, buff);
	_dbreak("FIN OP DISQUE\n");
}

void f_ping(void *args){
	_dprint("début de ping\n");
    while(1) {
		i++;
	}
	_dprint("fin de ping\n");
}

void f_pong(void *args){
	_dprint("début de pong\n");
    while(1) {
		i--;
	}
	_dprint("fin de pong\n");
} 

#include <signal.h>
int main(int argc, char *argv[]){
	boot(f_testDisk, NULL);
    return 0;
}
