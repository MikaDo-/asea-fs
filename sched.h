#ifndef CONTXT_H
#define CONTXT_H

#define dump_sp()   do{ \
                        void *ctx_esp, *ctx_ebp;                                \
                        asm("movq %%rsp, %0"                                    \
                            :"=r"(ctx_esp));                                    \
                        asm("movq %%rbp, %0"                                    \
                            :"=r"(ctx_ebp));                                    \
                                                                                \
                        printf("RSP : %x\n", (unsigned int) ctx_esp);           \
                        printf("RBP : %x\n----\n", (unsigned int) ctx_ebp);     \
                    }while(0);

#define CTX_MAGIC 0xdeadbeef

typedef void (*func_t)(void*);

typedef enum{
    CTXS_READY, CTXS_ACTIVABLE, CTXS_TERMINATED, CTXS_BLOCKED
} CtxStatus;

typedef struct{
    unsigned long long cpu_time;
    unsigned long long last_start;
} cpu_timing_str;

typedef struct ctx_str{
    void*    		ebp;
    void* 			esp;
    func_t 			func;
    void*			args;
    CtxStatus 		status;
    char* 			stack;
    int 			magic;
	char			name[20];
    struct ctx_str* prev; // j'ai fait une liste doublement chainée... Je ne sais plus pourquoi...
    struct ctx_str* next;
	unsigned int 	pid;
	struct ctx_str* semListNext;
    cpu_timing_str  timing;
    func_t 			onTerminate; // Utile pour éventuellement libérer des resources réservées pour le contexte.
    void* 			onTerminateParams; // Utile pour éventuellement libérer des resources réservées pour le contexte.
} ctx_s;

void sched_init();
void sched_purge_roundRobin();
void sched_remove_ctx(ctx_s *ctx);
unsigned int sched_get_ctx_count();
void sched_terminate_all();
int scheduleCtx(ctx_s* ctx);
ctx_s* create_ctx(int stackSize, func_t func, void *args);
void yield();
void switch_to_ctx(ctx_s* ctx);
int start_sched();
void ctx_set_name(ctx_s* ctx, const char* name);
void ctx_set_on_terminate(ctx_s* ctx, func_t func, void* params);
unsigned int ctx_get_pid(ctx_s* ctx);
void ctx_kill(unsigned int pid);

//fonctions de test de l'ordonnanceur
void f_ping(void *args);
void f_pong(void *args);
void f_testDisk(void *args);


#endif
