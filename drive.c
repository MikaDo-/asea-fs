#include "stdio.h"
#include "stdlib.h"
#include "stdint.h"
#include "string.h"
#include "unistd.h"
#include "signal.h"
#include "hardware.h"

#include "debug_tools.h"
#include "drive.h"
#include "frmt.h"
#include "vol.h"
#include "mbr.h"
#include "sched.h"
#include "disk.h"

mbr_str MBR;
unsigned int secSize = 0;
unsigned int secsPerCyl = 0;

extern char disk_running; // nous vient de très loin: disk.c et nous indique si on doit utiliser sleep(HDA_IRQ) ou si on considère qu'on utilise la FSM sur interruption de disk.c

void mSeek(unsigned int cyl, unsigned int sec){
    // Data
    _out(HDA_DATAREGS,   cyl >> 8);
	_out(HDA_DATAREGS+1, cyl & 0xff);
	_out(HDA_DATAREGS+2, sec >> 8);
	_out(HDA_DATAREGS+3, sec & 0xff);
	// Command
	_out(HDA_CMDREG,  CMD_SEEK);
	if(!disk_running)
		_sleep(HDA_IRQ);
}

void mFormat(unsigned int cyl, unsigned int sect, unsigned int nbSec, unsigned int val){
    _out(HDA_DATAREGS,     nbSec >>   8);
    _out(HDA_DATAREGS + 1, nbSec & 0xff);
	_out(HDA_DATAREGS + 2, val   >>  24);
	_out(HDA_DATAREGS + 3, val   >>  16);
	_out(HDA_DATAREGS + 4, val   >>   8);
	_out(HDA_DATAREGS + 5, val   >>   0);
    _out(HDA_CMDREG,   CMD_FORMAT);
	if(!disk_running)
		_sleep(HDA_IRQ);
}

void format_sector(unsigned int cylinder, unsigned int sector, unsigned int nSector){
   	unsigned int nbcyl, nbsec, taillesec;
	nbcyl = nbsec = taillesec = 0;
	getGeometry(&nbcyl, &nbsec, &taillesec);

	unsigned int currentCyl, currentSec;
	currentCyl = cylinder;
	currentSec = sector;

	for(int i = 0;i<nSector;i++){	
		if(currentSec >= nbsec){
			currentSec %= nbsec;
			currentCyl++;
		}
		if(currentCyl >= nbcyl)
			currentCyl %= nbcyl;

		if(!disk_running){
			_sleep(HDA_IRQ);
			mSeek(currentCyl, currentSec);
			mFormat(currentCyl, currentSec, 1, 0x00);
		}else
			disk_format(currentCyl, currentSec);
		currentSec++;
    }
}

void initDisk(){
	_out(HDA_CMDREG, CMD_DSKINFO);
	secSize  = 0;
	secSize	 = _in(HDA_DATAREGS + 4) << 8;
	secSize |= _in(HDA_DATAREGS + 5);
}

void getGeometry(unsigned int *nbCyl, unsigned int *nbSec, unsigned int* tailleSec){
	_out(HDA_CMDREG, CMD_DSKINFO);

	*nbCyl 		= _in(HDA_DATAREGS) << 8;
	*nbCyl 	   |= _in(HDA_DATAREGS+1);

	*nbSec 		= _in(HDA_DATAREGS + 2) << 8;
	*nbSec 	   |= _in(HDA_DATAREGS + 3);

	*tailleSec 	= _in(HDA_DATAREGS + 4) << 8;
	*tailleSec |= _in(HDA_DATAREGS + 5);

	secSize = *tailleSec;
	secsPerCyl = *nbSec;
}

void read_sector(unsigned int cylinder, unsigned int sector, unsigned char* buffer){
	if(!disk_running)
		mSeek(cylinder, sector);
	_out(HDA_DATAREGS, 0); // le nb de secteurs est sur 16 bits, donc 2 _out dont un à 0.
	_out(HDA_DATAREGS+1, 1);
	_out(HDA_CMDREG,  CMD_READ);
	if(!disk_running){
		_sleep(HDA_IRQ);
		memcpy(buffer, MASTERBUFFER, secSize); // TETE DE CON D'ALEXIS
	}
	return;
}

void write_sector(unsigned int cylinder, unsigned int sector, const unsigned char* buffer){
	if(!disk_running)
		mSeek(cylinder, sector);
	memcpy(MASTERBUFFER, buffer, secSize);
	_out(HDA_DATAREGS,   0x00);
	_out(HDA_DATAREGS+1, 0x01);
	_out(HDA_CMDREG,  CMD_WRITE);
	if(!disk_running)
		_sleep(HDA_IRQ);
}

