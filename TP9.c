#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hardware.h"
#include "drive.h"
#include "vol.h"
#include "mbr.h"
#include "inode.h"
#include "ifile.h"
#include "dir.h"
#include "mount.h"

extern mbr_str MBR;
extern unsigned int secSize;
extern superblock_str currentSuperblock;

extern unsigned int currentVol;

/*
 * Test des couches 4 & 5
 */

int main(int argc, char **argv)
{
	printf("\nGestion des fichiers par leurs inombres - ASE TP 9\n==================================================\n						Alexis Beaujet M1\n\n\n");
    
    mount();
    
    //makefs(currentVol);
    /*int err = 0;
        
    // fin des préparatifs d'usage.
    unsigned int newiFile;
    printf("create_ifile(FT_FILE) : %u\n", create_ifile(FT_FILE));
    printf("create_ifile(FT_FILE) : %u\n", (newiFile = create_ifile(FT_FILE)));
    delete_ifile(newiFile);
    printf("create_ifile(FT_FILE) : %u\n", create_ifile(FT_FILE));
    printf("create_ifile(FT_FILE) : %u\n", create_ifile(FT_FILE));
    printf("create_ifile(FT_FILE) : %u\n", (newiFile = create_ifile(FT_FILE)));
    
    print_inode(newiFile);
    
    delete_ifile(newiFile);
    printf("create_ifile(FT_FILE) : %u\n", create_ifile(FT_FILE));
    
    file_desc_str file;
    printf("open_ifile(file, newiFile) => %d\n", open_ifile(&file, newiFile));
    printf("flush_ifile(&file) => %d\n", flush_ifile(&file));
    
    writec_ifile(&file, 'v');
    char* buff = (char*)malloc(secSize);
    char phraseTest[] = "Je suis une chaine de test. LOL.";
    buff = strcpy(buff, phraseTest);
    
    int i=0;    
    while ( (err = write_ifile(&file, buff, 256)) > 0 ){
        //print_inode(newiFile);
        printf("%2d - ecriture... %d octets ecrits.\n", i++, err);
        printf("Volume %s plein\n", (isVolFull() == 0) ? "NON ":"");
        printf("Espace restant : %d/%d (1 bloc reserve au superbloc)\n", volFreeSpace(), MBR.volumes[currentVol].size);
    }
    
    printf("Volume %s plein\n", (isVolFull() == 0) ? "NON ":"");
    printf("Espace restant : %d/%d (1 bloc reserve au superbloc)\n", volFreeSpace(), MBR.volumes[currentVol].size);
    print_inode(newiFile);
    
    printf("Le fichier occupe tout le volume.\n");
    printf("close_ifile(&file) => %d\n", close_ifile(&file));
    
    printf("Ecriture terminee.\n\nMaintenant, la lecture : \n");
    
    printf("open_ifile(file, newiFile) => %d\n", open_ifile(&file, newiFile));
    read_ifile(&file, buff, 40);
    printf("Contenu lu dans le fichier : %s\n", buff);
    printf("close_ifile(&file) => %d\n", close_ifile(&file));*/
	
	char choix[2];
	do{
		printf("1. Ajouter entrees\n");
		printf("2. Lister entrees\n");
		printf("3. Supprimer entrees\n");

		scanf("%c", choix);
		choix[1] = 0;
		*choix = atoi(choix);
	}while(*choix < 0 || *choix > 3);

	if(*choix == 1){
		/* 
		 * On veut recréer la structure suivante : 
		 * /
		 * |-test0.txt
		 * | test1
		 * | |-test1.0.txt
		 * | |-test1.1.txt
		 * |-test2.txt
		 *
		 * On crée d'abord les fichiers+répertoires, puis ajoute les références vers les fichiers dans les répertoires.
		 */
		unsigned int test0  = create_ifile(FT_FILE);
		unsigned int test1  = create_ifile(FT_DIRECTORY);
		unsigned int test10 = create_ifile(FT_FILE);
		unsigned int test11 = create_ifile(FT_FILE);
		unsigned int test2  = create_ifile(FT_FILE);

		printf("Ajout de l'entree %u dans le repertoire test1 : %d\n", test10, add_entry(test1, test10, "test1.0.txt"));
		printf("Ajout de l'entree %u dans le repertoire test1 : %d\n", test11, add_entry(test1, test11, "test1.1.txt"));
		printf("Ajout de l'entree %u dans le repertoire racine : %d\n", test0, add_entry(1, test0, "test0"));
		printf("Ajout de l'entree %u dans le repertoire racine : %d\n", test1, add_entry(1, test1, "test1"));
		printf("Ajout de l'entree %u dans le repertoire racine : %d\n", test2, add_entry(1, test2, "test2"));
 	}else if(*choix == 2){
		printf("display entries : %d\n", display_entries(1));
		printf("display entries : %d\n", display_entries(3));
	}else if(*choix == 3){
		printf("del entries : %d\n", del_entry(1, "loule"));
		printf("del entires : %d\n", del_entry(1, "loyle"));
	}

    umount();
	exit(EXIT_SUCCESS);
}



