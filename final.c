#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hardware.h"
#include "drive.h"
#include "vol.h"
#include "mbr.h"
#include "inode.h"
#include "ifile.h"
#include "mount.h"

extern unsigned int currentVol;

/*
 * Test du système de fichiers final
 */

int main(int argc, char **argv){
	printf("\nTest du systeme de fichiers complet - ASE\n=========================================\n						Alexis Beaujet M1\n\n\n");
    mount();
    
	// prg    
        
    umount();
	exit(EXIT_SUCCESS);
}

