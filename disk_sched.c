#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <assert.h>
#include <signal.h>

#include "hardware.h"
#include "debug_tools.h"
#include "timer.h"
#include "sched.h"
#include "disk.h"
#include "disk_sched.h"
#include "drive.h"
#include "frmt.h"
#include "vol.h"
#include "mbr.h"
#include "mount.h"
#include "inode.h"
#include "ifile.h"
#include "dir.h"

#define DISK_MODE_NONE		0
#define DISK_MODE_SEMAPHORE 1
#define DISK_MODE_SCHEDULED 2

extern unsigned int secSize;
extern ctx_s	   *ctx_current;

ctx_s* 			disk_ctx 	= NULL;
disk_job_str* 	disk_queue 	= NULL; // le job en cours est celui en tête de liste.
disk_state_e  	disk_state 	= DS_IDLE;


// à ne pas confondre avec l'initialisatio du matériel.
// à faire en cas de reset
void disk_sched_init(){
	disk_running = 0;
	disk_purge_queue(disk_queue);
	if(disk_ctx)
		sched_remove_ctx(disk_ctx);
	disk_ctx = create_ctx(65535000, disk_check_queue, NULL);
	ctx_set_name(disk_ctx, "disk_ctx");
	disk_state = DS_IDLE;
	IRQVECTOR[HDA_IRQ] = disk_sched_irq_handler;
	disk_running = DISK_MODE_SCHEDULED;
}

// nettoie la liste des jobs en cours
// à faire en cas de reset
void disk_purge_queue(disk_job_str* queue){
	if(!queue)
		return;
	disk_purge_queue(queue->next);
	free(queue);
}

// on pop un élément de la queue et on le traite.
void disk_process_queue(){
	assert(disk_queue);	
	switch(disk_queue->type){
		case DJT_READ:
		case DJT_WRITE:
		case DJT_FORMAT:
			// lancer seek;
			mSeek(disk_queue->cyl, disk_queue->sect);
			disk_state = DS_SEEK;
			_dprint("SEEK LANCE\n");
			disk_ctx->status = CTXS_BLOCKED;
			disk_queue->ctx->status = CTXS_BLOCKED;
			yield();
			break;
		default:
			_print_err("Oups oups...\n");
			exit(65454);
	}
}

#warning TODO: Ceci est une pile, pas une file !
void disk_schedule_job(disk_job_str *job){
	assert(disk_ctx);
	assert(job);

    job->ctx->status = CTXS_BLOCKED;

	if(!disk_queue)
		disk_queue = job;
	else{
		job->next = disk_queue;
		disk_queue = job;
	}
	disk_ctx->status = 
		(disk_ctx->status == CTXS_READY) ? CTXS_READY : CTXS_ACTIVABLE; // PUTAIN BORDEL DE MERDE 3H SUR CE PUTAIN DE BRANLAGE DE NAIN
		// On ne veut pas passer le contexte de READY à ACTIVABLE sans qu'il ait été élu une fois.
}

void disk_delete_job(disk_job_str* job){
	assert(job);
	disk_queue = job->next;
	free(job);
}

void disk_check_queue(){
	while(true){
		_dprint("check_queue\n");
		if(disk_queue){
			_dprint(" OUI\n");
			disk_process_queue();
		}else{
			_dprint(" NON\n");
			disk_ctx->status = CTXS_BLOCKED;
			yield();
		}
	}
}

disk_job_str* disk_create_job(unsigned int cyl, unsigned int sect){
	disk_job_str* nv = (disk_job_str*)malloc(sizeof(disk_job_str));
	nv->cyl = cyl;
	nv->sect = sect;
	nv->buffer = NULL;
	nv->ctx = ctx_current;
	nv->next = NULL;
	return nv;
}

void disk_sched_write(unsigned int cyl, unsigned int sect, unsigned char* buffer){
	disk_job_str* nv = disk_create_job(cyl, sect);
	nv->buffer = buffer;
	nv->type = DJT_WRITE;
	disk_schedule_job(nv);
	yield();
}

void disk_sched_read(unsigned int cyl, unsigned int sect, unsigned char* buffer){
	disk_job_str* nv = disk_create_job(cyl, sect);
	nv->buffer = buffer;
	nv->type = DJT_READ;
	disk_schedule_job(nv);
	yield();
}

void disk_sched_format(unsigned int cyl, unsigned int sect){
	disk_job_str* nv = disk_create_job(cyl, sect);
	nv->type = DJT_FORMAT;
	disk_schedule_job(nv);
	yield();
}

// FSM
void disk_sched_irq_handler(){
	irq_disable();
	disk_job_str* current = disk_queue;
assert(current);
	ctx_s* owner = current->ctx;
	_dprint("DISK_IRQ\n");

	switch(disk_state){
		case DS_IDLE:
			fprintf(stderr, "WARNING - %s:%d - UNEXPECTED INTERRUPT IN IDLE STATE.\n", __FILE__, __LINE__);
			return;
		case DS_SEEK:{
			_dprint("SEEK TERMINE\n");
			// on vient de finir le seek.
			// Que faire maintenant ? > Dépend du type de tâche	
			switch(current->type){
				case DJT_READ:
					// on lance le read
					disk_state = DS_READ;
					read_sector(current->cyl, current->sect, current->buffer);
					irq_enable();
					return;
				case DJT_WRITE:
					// on lance le write
					disk_state = DS_WRITE;
					write_sector(current->cyl, current->sect, current->buffer);
					irq_enable();
					return;
				case DJT_FORMAT:
					// on lance le format
					disk_state = DS_FORMAT;
					mFormat(current->cyl, current->sect, 1, 0x0);
					irq_enable();
					return;
				default:
					// ???
					return;
			}
		}
		case DS_READ: // les données sont présentes dans le MASTERBUFFER. On les copie dans l buffer du job
			_dprint("READ TERMINE\n");
			assert(current->buffer);
			memcpy(current->buffer, MASTERBUFFER, secSize);
		case DS_WRITE:
		case DS_FORMAT:
			//disk_delete_job(current);
			disk_queue = current->next;

			// retour à idle
			disk_state = DS_IDLE;
			disk_ctx->status = CTXS_ACTIVABLE;
			// réactiver contexte propriétaire.
            owner->status = CTXS_ACTIVABLE;
			_dprint("JOB DONE\n");
			irq_enable();
			return;
		default:
			return;
	}
}

