#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>
#include <signal.h>

#include "hardware.h"
#include "debug_tools.h"
#include "sched.h"
#include "disk.h"
#include "disk_sched.h"

char disk_running			= DISK_MODE_NONE;

void disk_init(char disk_mode){
	switch(disk_mode){
		case DISK_MODE_SEMAPHORE:
			//disk_sem_init();
			break;
		case DISK_MODE_SCHEDULED:
			disk_sched_init();
			break;
		default:
			_print_err("You messed up big time asshole.\n");
	}
}
void disk_write(unsigned int cyl, unsigned int sect, unsigned char* buffer){
	switch(disk_running){
		case DISK_MODE_SEMAPHORE:
			//disk_sem_write(cyl, sect, buffer);
			break;
		case DISK_MODE_SCHEDULED:
			disk_sched_write(cyl, sect, buffer);
			break;
		default:
			_print_err("You messed up big time asshole.\n");
	}
}
void disk_read(unsigned int cyl, unsigned int sect, unsigned char* buffer){
	switch(disk_running){
		case DISK_MODE_SEMAPHORE:
			//disk_sem_read(cyl, sect, buffer);
			break;
		case DISK_MODE_SCHEDULED:
			disk_sched_read(cyl, sect, buffer);
			break;
		default:
			_print_err("You messed up big time asshole.\n");
	}
}
void disk_format(unsigned int cyl, unsigned int sect){
	switch(disk_running){
		case DISK_MODE_SEMAPHORE:
			//disk_sem_format(cyl, sect);
			break;
		case DISK_MODE_SCHEDULED:
			disk_sched_format(cyl, sect);
			break;
		default:
			_print_err("You messed up big time asshole.\n");
	}
}

