#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hardware.h"
#include "drive.h"
#include "vol.h"
#include "mbr.h"
#include "inode.h"
#include "ifile.h"
#include "mount.h"

extern mbr_str MBR;
extern unsigned int secSize;
extern superblock_str currentSuperblock;
extern unsigned int currentVol;
unsigned int nbSec, nbCyl;

/*
 * Test des couches 4 & 5
 */
 
int mount(){
    int err = 0;
    char *envtmp=NULL;

    if( (err = startDisk()) )
        return err;

    if(!(envtmp = getenv("CURRENT_VOLUME")) ){
        envtmp = (char*)malloc(4);
        sprintf(envtmp, "%s", "0");
    }
    currentVol = atoi(envtmp);
    printf("INFO  - Selected volume : %d\n", currentVol);
    
    if( (err = load_superblock(currentVol)))
        printf("ERREUR : Impossible de charger le volume %d : err %d\n", currentVol, err);
    else
        dump_superblock();
    return err;
}

void it(){}

int initConfig(){
    char *hwConfig=NULL;
    hwConfig = getenv("HW_CONFIG");
    if(!hwConfig){
        hwConfig = (char*)malloc(255);
        sprintf(hwConfig, "%s", "hardware.ini");
    }
    printf("INFO  - hwpath : %s\n", hwConfig);
    if(init_hardware(hwConfig) == 0) {
	    fprintf(stderr, "Error in hardware initialization\n");
    	exit(EXIT_FAILURE);
    }
	for(int i = 0;i<16;i++)
		IRQVECTOR[i] = it;
    return 0;
}

int startDisk(){
	initDisk(); // se charge d'initialiser l'IRQ vector, récupérer taille de secteur...
	getGeometry(&nbCyl, &nbSec, &secSize);
	printf("INFO  - Chargement de la table des partitions...");
	mbr_load();
	if(checkMBR() >=0 && MBR.volCount <= 8)
		printf("    [OK]\n\n");
	else{
		printf("    [ECHEC]\n");
		printf("ERROR - La table des partitions est illisible ou non initialisee. Il est necessaire d'en recreer une nouvelle.\n\n");
        return -1;
	}
    return 0;
}

void umount(){
    save_superblock();
}

