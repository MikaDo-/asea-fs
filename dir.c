/* ------------------------------
   $Id: dir.c 7085 2013-10-18 15:37:14Z marquet $
   ------------------------------------------------------------

   Directories manipulation.
   Based on the ifile library.    
   Philippe Marquet, october 2002

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include "vol.h"
#include "mbr.h"
#include "inode.h"
#include "ifile.h"
#include "dir.h"
#include "debug_tools.h"

#define MIN(X, Y) ((X) < (Y) ? (X) : (Y))

// copie de chaine de caractères
char * strdup(const char *s){
    size_t siz;
    char *copy;
    
    siz = strlen(s) + 1;
    if ((copy = malloc(siz)) == NULL)
		return(NULL);
    (void)memcpy(copy, s, siz);
    return(copy);
}

// petite focntion temporaire pour tester les dossiers... à supprimer à l'avenir.
int testDossiers(){
	unsigned int dir = create_directory();
	display_entries(dir);
	
	entry_str entr;
	
	file_desc_str desc;
	open_ifile(&desc, dir);
		entr.inumber = 15;
		sprintf(entr.basename, "%s", "premier");
		printf("write 1 : %d\n", write_ifile(&desc, &entr, sizeof(entry_str)));
		entr.inumber = 77;
		sprintf(entr.basename, "%s", "deuxieme");
		printf("write 2 : %d\n", write_ifile(&desc, &entr, sizeof(entry_str)));
		entr.inumber = 55;
		sprintf(entr.basename, "%s", "troisieme");
		printf("write 3 : %d\n", write_ifile(&desc, &entr, sizeof(entry_str)));
	close_ifile(&desc);

	display_entries(dir);
	return 0;
}

unsigned int create_directory(file_desc_str *dir_descriptor){
    int err;
    if( (err = create_ifile(FT_DIRECTORY)) == 0 )
        fprintf(stderr, "ERROR - %s:%d - CANNOT CREATE DIRECTORY-FILE INODE.\n", __FILE__, __LINE__ );
    return err;
}


/*------------------------------
  Private entry manipulation.
  ------------------------------------------------------------*/

/* return a new entry index.
   the file descriptor must the one of an open directory */
unsigned int new_entry(file_desc_str *fd){
    entry_str entry;
    unsigned int ientry = 0; /* the new entry index */

    /* seek to begin of dir */
    seek2_ifile(fd, 0);
    
    /* look after a null inumber in an entry */
    while (read_ifile (fd, &entry, sizeof(entry_str)) != READ_EOF) {
		if (! entry.inumber) // l'entrée est libre si l'inumber de cette entree vaut 0
			return ientry;
		ientry++;
    }
    
	return ientry;
}

/* return the entry index or -1.
   the file descriptor must the one of an open directory */
int find_entry(file_desc_str *fd, const char *basename){
    entry_str entry;
    unsigned int ientry = 0;

    seek2_ifile(fd, 0);
    int err;
    while ((err = read_ifile (fd, &entry, sizeof(entry_str))) != READ_EOF) {
		if(err<0){
			fprintf(stderr, "ERROR - %s:%d - Cannot read directory %u while reading its entries.\n", __FILE__, __LINE__, fd->inumber);
			return -1;
		}
		if (entry.inumber && !strcmp(entry.basename, basename))
			return ientry;
		ientry++;
    }

    return -1;
}



/*------------------------------
  Create and delete entry
  ------------------------------------------------------------*/
int add_entry(unsigned int idir, unsigned int inumber, const char *basename) {
    inode_str inode; 
    file_desc_str _fd, *fd = &_fd;
    entry_str entry;
    unsigned int ientry = 0;
    int nbyte; 
    
    /* a directory inode? */
    read_inode(idir, &inode); 
    if (inode.type != FT_DIRECTORY){ 
		fprintf(stderr, "ERRROR - %s:%d - CANNOT REGISTER A FILE IN A NON-DIRECTORY FILE.\n   --->basename: %s\n     --->inumber: %u\n", __FILE__, __LINE__, basename, inumber);
		return -1;
	}
    
    /* open the dir */
    open_ifile(fd, idir);

    /* the new entry position in the file */
    ientry = new_entry(fd);
    
    /* built the entry */
    entry.inumber = inumber;
    strncpy(entry.basename, basename, BASENAME_MAX_SIZE);
    entry.basename[BASENAME_MAX_SIZE - 1] = 0;

    /* seek to the right position */
    seek2_ifile(fd, ientry * sizeof(entry_str));
    
    /* write the entry */
    nbyte = write_ifile(fd, &entry, sizeof(entry_str));
    
    /* done */
    close_ifile(fd); /* even in case of write failure */

    if (nbyte == sizeof(entry_str))
		return 0;
    else
		return -1;
}

int del_entry(unsigned int idir, const char *basename){
    inode_str inode; 
    file_desc_str _fd, *fd = &_fd;
    entry_str entry; 
    unsigned int ientry;
    int status;
    
    /* a directory inode? */
    read_inode(idir, &inode); 
    if (inode.type != FT_DIRECTORY) 
		return -1;
    
    /* open the dir */
    open_ifile(fd, idir);

    /* the entry position in the file */
    status = find_entry(fd, basename);
    if (status == -1) {
		close_ifile(fd);
		return -1;
    }
    ientry = status; 

    /* built a null entry */
    memset(&entry, 0, sizeof(entry_str));
    
    /* seek to that entry */
    seek2_ifile(fd, ientry * sizeof(entry_str));

    /* delete the entry = write the null entry */
    status = write_ifile(fd, &entry, sizeof(entry_str));

    /* close, and report status */
    close_ifile(fd);
    return status;
}

/*------------------------------
  Looking after entries
  ------------------------------------------------------------*/

/* consider the directory of inumber idir. 
   search after an entry of name basename (which can not contain /). 
   return the inumber of the entry, 0 if no such entry or if idir is
   not a directory.
*/
unsigned int inumber_of_basename(unsigned int idir, const char *basename){
    inode_str inode;
    file_desc_str _fd, *fd = &_fd;
    unsigned int ientry;
    entry_str entry;
    int status;
    
    /* a directory inode? */
    read_inode(idir, &inode);
    if (inode.type != FT_DIRECTORY) 
		return 0;

    /* open the dir */
    open_ifile(fd, idir);

    /* the entry position in the file */
    status = find_entry(fd, basename);
    if (status < 0)  // attention: ce n'est pas l'inumber ici mais l'index de l'entrée dans l'inode !!!!!!!
		return 0;
    ientry = status; 

    /* seek to the right position */
    seek2_ifile(fd, ientry * sizeof(entry_str));
    
    /* read the entry */
    status = read_ifile(fd, &entry, sizeof(entry_str));
    close_ifile(fd); 

    /* the result */
    return entry.inumber;
}

unsigned int inumber_of_path(const char *pathname){
    unsigned int icurrent;	/* the inumber of the current dir */
    
    /* an *absolute* pathname */
    if (*pathname != '/')
	return 0;

    /* start at root */
    icurrent = 1;
    
    while (*pathname) {
		if (*pathname != '/') {
			char basename[BASENAME_MAX_SIZE];
			char *pos;		/* the first / position */
			int lg;			/* the length of the first basename */
			
			/* length of the leading basename */
			pos = strchr(pathname, '/');
			lg = pos ? pos - pathname : strlen (pathname);

			/* copy this leading basename to basename */
			strncpy (basename, pathname, MIN(BASENAME_MAX_SIZE, lg));
			basename[MIN(BASENAME_MAX_SIZE-1, lg)] = 0;

			/* look after this basename in the directory.
			   this entry inumber is the new current */
			icurrent = inumber_of_basename(icurrent, basename); 
			if (icurrent <= 0) 
				return icurrent;

			/* skip the basename in pathname */
			pathname += lg;

			/* may end here */ 
			if (! *pathname)
				break;
		}
		pathname++ ;
    }
    return icurrent ;
}

unsigned int dinumber_of_path(const char *pathname, const char **basename){
    char *dirname = strdup(pathname);
    unsigned int idirname = 0; 
    inode_str inode; 

    /* an *absolute* pathname */
    if (*pathname != '/') 
		goto free;
    
    /* the last basename (there is at least a '/') */
    *basename = strrchr (pathname, '/');
    (*basename)++;

    /* the dirname stops at the last '/'. ugly isn't it! */
    *(dirname + ((*basename) - pathname)) = 0;
    
    /* the dirname inumber */
    idirname = inumber_of_path(dirname);
    if (! idirname)
		goto free; 

    /* is dirname a directory? */
    read_inode(idirname, &inode); 
    if (inode.type != FT_DIRECTORY)
		idirname = 0; 
printf("DEBUG - %s:%d - DINUMBER OF PATH : DIRNAME = %s\n", __FILE__, __LINE__, dirname);
 free:
    /* free dirname strdup() */
    free(dirname); 
    return idirname;
}
// la dernière case du résultat est vide et indique la fin du tableau.
int getDirContents(unsigned int idir, fileinfo_str** infos, unsigned int *entryCount){

    // nombre d'entrées dans le répertoire = size / sizeof(entry_str)
    int err = 0;
    inode_str inode;
    if( (err = read_inode(idir, &inode)) ){
        fprintf(stderr, "ERROR - %s:%d CANNOT READ DIRECTORY INODE %u.\n", __FILE__, __LINE__ , idir);
        return err;
    }
	if(inode.type != FT_DIRECTORY){
		fprintf(stderr, "ERROR - %s:%d - FILE %u IS NOT A DIRECTORY.\n", __FILE__, __LINE__, idir);
		return -1;
	}

	*entryCount = inode.size / sizeof(entry_str);

	if(*entryCount == 0){
		printf("Directory empty.\n");
		return 0;
	}

	// on alloue le tableau de résultats
	*infos = (fileinfo_str*)calloc(*entryCount+1, sizeof(fileinfo_str));
    entry_str entry;
    file_desc_str desc;
    if( (err = open_ifile(&desc, idir)) ){
        fprintf(stderr, "ERROR - %s:%d CANNOT OPEN DIR FILE %u.\n", __FILE__, __LINE__ , idir);
        return err;
    }
    int eCount = *entryCount;
    for (int i = 0; i < eCount; i++){
        read_ifile(&desc, &entry, sizeof(entry_str));
		if(entry.inumber == 0){ // fichier supprimé
			*entryCount -= 1;
			i--;
			continue;
		}
		(*infos)[i].basename = (char*)malloc(strlen(entry.basename)+1);
		strcpy((*infos)[i].basename, entry.basename);
		(*infos)[i].inumber = entry.inumber;
		// on détermine le type et la taille de l'inode référencé
		inode_str inodeTmp;
		if( (err = read_inode(entry.inumber, &inodeTmp)) > 0){
			_printf_err("CANNOT DETERMINE INODE TYPE AND SIZE FOR ENTRY %u\n", entry.inumber);
			continue;
		}
		(*infos)[i].type = inodeTmp.type;
		(*infos)[i].size = inodeTmp.size;
    }
    return err;
}

int display_entries(unsigned int inumber){ // inumber du répertoire à explorer.
	fileinfo_str *infos = NULL;
	int err;
	unsigned int entryCount;
	if( (err = getDirContents(inumber, &infos, &entryCount)) )
		return err;
	int i = 0;
	printf("                    FILENAME    | INUM | FILE-TYPE |   SIZE\n");
	for(int j = 0;j<entryCount;j++){
		if(infos[i].type == FT_UNKNOWN)
			continue;
        printf("%30s  | %4u | ", infos[j].basename, infos[j].inumber);
		switch(infos[i].type){
			case FT_FILE:
				printf("  FILE    | ");
				i++;
				break;
			case FT_DIRECTORY:
				printf("DIRECTORY | ");
				i++;
				break;
			default:
				printf(" UNKNOWN  | ");
		}
		printf(" %5u B\n", infos[j].size);
	}
	printf("%d entrees au total.\n", i);
	return err;
}
