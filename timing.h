#ifndef TIMING_H_INCLUDED
#define TIMING_H_INCLUDED

static __inline__ unsigned long long rdtsc(void);

#endif
