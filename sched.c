#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <signal.h>
#include <string.h>

#include "debug_tools.h"
#include "hardware.h"
#include "timer.h"
#include "timing.h"

#include "sched.h"
#include "semaphore.h"

#if defined(__i386__)

static __inline__ unsigned long long rdtsc(void)
{
  unsigned long long int x;
     __asm__ volatile (".byte 0x0f, 0x31" : "=A" (x));
     return x;
}

#elif defined(__x86_64__)

static __inline__ unsigned long long rdtsc(void)
{
  unsigned hi, lo;
  __asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
  return ( (unsigned long long)lo)|( ((unsigned long long)hi)<<32 );
}

#endif

char sched_running = 0;		// indique à l'IRQ handler du timer si on doit yield ou pas.
ctx_s* ctx_current = NULL; 	// contexte actuel
ctx_s* roundRobin  = NULL; 	// boucle des contextes actifs
unsigned int pid_count = 0;

// à faire en cas de reset...
void sched_init(){
	sched_purge_roundRobin();
	ctx_current = NULL;
	sched_running = 0;
}

void sched_purge_roundRobin(){
	while(roundRobin)
		sched_remove_ctx(roundRobin);
}

void sched_remove_ctx(ctx_s *ctx){
	assert(ctx);
	ctx_s* prev = ctx->prev;
	ctx_s* next = ctx->next;

	// si le contexte en tête était le dernier
	// on le supprime et rr pointe sur NULL...
	if(ctx == next){
		free(ctx->stack);
		free(ctx);
		roundRobin = NULL;
		return;
	}
		
	next->prev = prev;
	prev->next = next;
	free(ctx->stack);
	free(ctx);
	if(roundRobin == ctx)
		roundRobin = next;
}

// insertion d'un context dans le round robin (en tête) 
int scheduleCtx(ctx_s* ctx){
    irq_disable();
    if(!roundRobin)
        ctx->prev = ctx->next = roundRobin = ctx;
	else{
		ctx_s* last = roundRobin->prev;
		ctx->next = roundRobin;
		roundRobin->prev = ctx;
		last->next = ctx;
		ctx->prev = last;
		roundRobin = ctx;
	}
    irq_enable();
    return 0;
}

ctx_s* create_ctx(int stackSize, func_t func, void *args){
	ctx_s* ctx = (ctx_s*)malloc(sizeof(ctx_s));
	
    ctx->stack = (char*)malloc(stackSize);
    if(!ctx->stack){
        irq_enable();
        return NULL;
    }
    ctx->ebp = ctx->esp = ctx->stack + stackSize - 4;
    ctx->magic = CTX_MAGIC;
    ctx->status = CTXS_READY;
    ctx->args = args;
    ctx->func = func;
    ctx->pid = pid_count++;
	ctx->semListNext = NULL;
    ctx->timing.cpu_time = 0;
    ctx->timing.last_start = 0;
    ctx->onTerminate = NULL;
    ctx->onTerminateParams = NULL;
    
    scheduleCtx(ctx);
    return ctx;
}

void ctx_set_on_terminate(ctx_s* ctx, func_t func, void* params){
	ctx->onTerminate = func;
	ctx->onTerminateParams = params;
}

unsigned int ctx_get_pid(ctx_s* ctx){
	return ctx->pid;
}

void ctx_set_name(ctx_s* ctx, const char* name){
	memcpy(ctx->name, name, 20);
}

void ctx_kill(unsigned int pid){
	ctx_s* ctx = ctx_current;
	do{
		if(ctx->pid == pid)
			ctx->status = CTXS_TERMINATED;
	}while( (ctx=ctx->next) != ctx_current);
}

unsigned int sched_get_ctx_count(){
	unsigned int count = 0;
	if(roundRobin == NULL)
		return 0;
	count = 1;
	ctx_s* pt = roundRobin;
	while( (pt = pt->next) && pt != roundRobin)
		count++;
	return count;
}

void sched_terminate_all(){
	ctx_s* pt = roundRobin;
	if(!pt)
		return;
	pt->status = CTXS_TERMINATED;
	putchar('.');
	while( (pt = pt->next) && pt != roundRobin){
		pt->status = CTXS_TERMINATED;
		putchar('.');
	}
}

void start_ctx(ctx_s* ctx){
    irq_enable();
	ctx->status = CTXS_ACTIVABLE;
    ctx->timing.last_start = rdtsc();
    ctx->func(ctx->args);
    ctx->status = CTXS_TERMINATED;
	_dprintf("Ctx %u:%s(%p) terminated. Total CPU usage %llu\n", ctx->pid, ctx->name, (void*)ctx, ctx->timing.cpu_time);
	if(ctx->onTerminate){
		_dprint("Proceeding to cleanup...\n");
		ctx->onTerminate(ctx->onTerminateParams);
		_dprint("Done.\n");
	}
    yield();
}

void switch_to_ctx(ctx_s* ctx){
    static ctx_s* ctxSwitch;
    irq_disable();
    ctxSwitch = ctx;

    while(	ctxSwitch->status != CTXS_ACTIVABLE &&
			ctxSwitch->status != CTXS_READY){
		ctx_s* next = ctxSwitch->next;
		// On peut se trouver dans le cas suivant pour plusieurs raisons.
		// Toutefois, cela vient la plupart du temps à cause du fait qu'un process
		// soit bloqué par le disque, et que le disque lui même soit bloqué en
		// attendant une IRQ. Donc une seule chose à faire... attendre...
		/*while(ctxSwitch == ctx_current && ctx_current->status == CTXS_BLOCKED)
            irq_enable();
            //_print_info("Attente IRQ pour débloquer la situation : Un seul contexte, et il est bloque.\n");
        irq_disable();*/
		// si le contexte est terminé, on le supprime.
		if(ctxSwitch->status == CTXS_TERMINATED){
			if(ctxSwitch == ctx_current){ // c'était le dernier contexte et il est maitnenant terminé : on quitte.
                printf("All contexts done !\n");
				exit(3);
            }else
                sched_remove_ctx(ctxSwitch);
		}
        ctxSwitch = next;
		//printf(".");
    }

	// il faut que le contexte soit valide (MAGIC), et son satut doit être soit ACTIVABLE soit READY.
    if(ctxSwitch->magic != CTX_MAGIC || (ctxSwitch->status != CTXS_READY && ctxSwitch->status != CTXS_ACTIVABLE)){  
		_dbreak("Oops...\n");
        irq_enable();
        return;
    }
	if(ctx_current){ // au démarrage de l'ordonnancement, on n'a pas encore de contexte actif.
		asm("movq %%rsp, %0"
			: "=r"(ctx_current->esp));
		asm("movq %%rbp, %0"
			: "=r"(ctx_current->ebp));// on sauvegarde ebp et esp dans le contexte actuel
    }
    asm("movq %0, %%rsp"
        :
        : "r"(ctxSwitch->esp));
    asm("movq %0, %%rbp"
        :
        : "r"(ctxSwitch->ebp)); // on fait pointer ebp et esp sur la pile du contexte vers lequel on switch

    ctx_current = ctxSwitch;
    
	ctx_current->timing.last_start = rdtsc();
    if(ctx_current->status == CTXS_READY)
        start_ctx(ctx_current);
    else
        irq_enable();
}

// yield sauvegarde le contexte dans la case correspondante du RR et charge le contexte suivant. (switch_to(current->next))
void yield(){
    assert(roundRobin);
    if(ctx_current == NULL)
        switch_to_ctx(roundRobin);
    else{
        unsigned long long tdiff = rdtsc()-ctx_current->timing.last_start;
        ctx_current->timing.cpu_time += tdiff;
		//_dprintf("ctx %s(0x%p) : %llu cycles\n", ctx_current->name, ctx_current, tdiff);

    	switch_to_ctx(ctx_current->next);
	}
}

int start_sched(){
    if(roundRobin == NULL) // si on n'a aucun contexte à activer, on retourne...
        return -1;
	sched_running = 1;
    assert(roundRobin);
    switch_to_ctx(roundRobin);
    return 0;
}

