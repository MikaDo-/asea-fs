#ifndef DISK_SCHED_H_INCLUDED
#define DISK_SCHED_H_INCLUDED

typedef enum disk_job_type_e{
	DJT_READ, DJT_WRITE, DJT_FORMAT
}disk_job_type_e;


typedef enum disk_state_e{
	DS_IDLE, DS_SEEK, DS_READ, DS_WRITE, DS_FORMAT
}disk_state_e;

typedef struct disk_job_str{
	disk_job_type_e			 type;
	struct disk_job_str		*next;
	void					*buffer;
	unsigned int			 cyl;
	unsigned int			 sect;	
	ctx_s					*ctx;
}disk_job_str;

void disk_sched_init();
void disk_purge_queue(disk_job_str* queue);
void process_queue(void);
void next_state(void);
void disk_sched_irq_handler();
void disk_process_queue();
void disk_schedule_job(disk_job_str *job);
void disk_check_queue();
disk_job_str* disk_create_job(unsigned int cyl, unsigned int sect);
void disk_sched_write(unsigned int cyl, unsigned int sect, unsigned char* buffer);
void disk_sched_read(unsigned int cyl, unsigned int sect, unsigned char* buffer);
void disk_sched_format(unsigned int cyl, unsigned int sect);
#endif
