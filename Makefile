# $Id: Makefile,v 1.1 2002/12/06 14:25:18 marquet Exp $
##############################################################################

#ROOTDIR=/home/enseign/ASE
#ROOTDIR=/home/varamox/documents/Fac/M1/ASE/RES
ROOTDIR=.

CC	= gcc
CFLAGS	= -Wall -pedantic -std=gnu99 -g -O0
LIBDIR  = -L$(ROOTDIR)/lib
INCDIR  = -I$(ROOTDIR)/include
LIBS    = -lhardware -lm -lreadline

###------------------------------
### Main targets 
###------------------------------------------------------------
BINARIES= mkhd vm dfs mkfs mkvol dvol if_pfile if_nfile if_cfile if_dfile bsh
OBJECTS	= $(addsuffix .o, $(BINARIES))
#OBJECTS	= $(addsuffix .o,\
#	  drive mbr vol TP8 dfs mkfs mkvol dvol TP9 if_pfile if_nfile if_cfile if_dfile bsh sched_test)

all: $(BINARIES) $(OBJECTS)

###------------------------------
### Binaries
###------------------------------------------------------------
mkhd: mkhd.o
	$(CC) $(CFLAGS) -o bin/$@ $^ $(LIBDIR) $(LIBS)
vm: vm.o mbr.o drive.o vol.o ifile.o inode.o disk.o disk_sched.o sched.o boot.o timer.o semaphore.o mount.o
	$(CC) $(CFLAGS) -o bin/$@ $^ $(LIBDIR) $(LIBS)
dvol: dvol.o mbr.o drive.o vol.o ifile.o inode.o disk.o disk_sched.o sched.o boot.o timer.o semaphore.o mount.o
	$(CC) $(CFLAGS) -o bin/$@ $^ $(LIBDIR) $(LIBS)
dfs: dfs.o mbr.o drive.o vol.o ifile.o inode.o disk.o disk_sched.o sched.o boot.o timer.o semaphore.o mount.o
	$(CC) $(CFLAGS) -o bin/$@ $^ $(LIBDIR) $(LIBS)
mkfs: mkfs.o mbr.o drive.o vol.o ifile.o inode.o disk.o disk_sched.o sched.o boot.o timer.o semaphore.o mount.o
	$(CC) $(CFLAGS) -o bin/$@ $^ $(LIBDIR) $(LIBS)
mkvol: mkvol.o mbr.o drive.o vol.o ifile.o inode.o disk.o disk_sched.o sched.o boot.o timer.o semaphore.o mount.o
	$(CC) $(CFLAGS) -o bin/$@ $^ $(LIBDIR) $(LIBS)

if_pfile: if_pfile.o mount.o ifile.o inode.o mbr.o drive.o vol.o disk.o disk_sched.o sched.o boot.o timer.o semaphore.o
	$(CC) $(CFLAGS) -o bin/$@ $^ $(LIBDIR) $(LIBS)
if_nfile: if_nfile.o dir.o mount.o ifile.o inode.o mbr.o drive.o vol.o disk.o disk_sched.o sched.o boot.o timer.o semaphore.o
	$(CC) $(CFLAGS) -o bin/$@ $^ $(LIBDIR) $(LIBS)
if_cfile: if_cfile.o mount.o ifile.o inode.o mbr.o drive.o vol.o disk.o disk_sched.o sched.o boot.o timer.o semaphore.o
	$(CC) $(CFLAGS) -o bin/$@ $^ $(LIBDIR) $(LIBS)
if_dfile: if_dfile.o mount.o ifile.o inode.o mbr.o drive.o vol.o disk.o disk_sched.o sched.o boot.o timer.o semaphore.o
	$(CC) $(CFLAGS) -o bin/$@ $^ $(LIBDIR) $(LIBS)
bsh: bsh.o dir.o mount.o ifile.o inode.o mbr.o drive.o vol.o boot.o sched.o timer.o disk.o disk_sched.o
	$(CC) $(CFLAGS) -o bin/$@ $^ $(LIBDIR) $(LIBS)

###------------------------------
### #include dependences 
###------------------------------------------------------------
# you may fill these lines with "make depend"

TP8.o: TP8.c hardware.h drive.h vol.h mbr.h inode.h mount.h
TP9.o: TP9.c hardware.h drive.h vol.h mbr.h inode.h ifile.h dir.h mount.h
boot.o: boot.c debug_tools.h hardware.h timer.h drive.h mount.h sched.h \
 disk.h
bsh.o: bsh.c hardware.h mount.h inode.h ifile.h dir.h sched.h boot.h \
 vol.h debug_tools.h
dfs.o: dfs.c hardware.h drive.h mount.h vol.h mbr.h
dir.o: dir.c vol.h mbr.h inode.h ifile.h dir.h debug_tools.h
disk.o: disk.c hardware.h debug_tools.h sched.h disk_sched.h
disk_sched.o: disk_sched.c hardware.h debug_tools.h timer.h sched.h \
 disk.h disk_sched.h drive.h frmt.h vol.h mbr.h mount.h inode.h ifile.h \
 dir.h
drive.o: drive.c hardware.h debug_tools.h drive.h frmt.h vol.h mbr.h \
 sched.h disk.h
dumps.o: dumps.c hardware.h drive.h mount.h
dvol.o: dvol.c hardware.h drive.h vol.h mbr.h
final.o: final.c hardware.h drive.h vol.h mbr.h inode.h ifile.h mount.h
frmt.o: frmt.c hardware.h drive.h
if_cfile.o: if_cfile.c hardware.h drive.h vol.h mbr.h inode.h ifile.h \
 mount.h
if_dfile.o: if_dfile.c hardware.h drive.h vol.h mbr.h inode.h ifile.h \
 mount.h
if_nfile.o: if_nfile.c hardware.h drive.h vol.h mbr.h inode.h ifile.h \
 mount.h dir.h
if_pfile.o: if_pfile.c hardware.h drive.h vol.h mbr.h inode.h ifile.h \
 mount.h
ifile.o: ifile.c hardware.h drive.h vol.h mbr.h inode.h ifile.h
inode.o: inode.c hardware.h drive.h vol.h mbr.h inode.h debug_tools.h
mbr.o: mbr.c hardware.h drive.h sched.h disk.h vol.h mbr.h
mkfs.o: mkfs.c hardware.h debug_tools.h mount.h drive.h vol.h mbr.h
mkhd.o: mkhd.c hardware.h
mkvol.o: mkvol.c hardware.h drive.h vol.h mbr.h
mount.o: mount.c hardware.h drive.h vol.h mbr.h inode.h ifile.h mount.h
mutex.o: mutex.c sched.h semaphore.h mutex.h
sched.o: sched.c debug_tools.h hardware.h timer.h timing.h sched.h \
 semaphore.h
sched_test.o: sched_test.c debug_tools.h sched.h boot.h timer.h disk.h
semaphore.o: semaphore.c hardware.h timer.h sched.h semaphore.h
timer.o: timer.c debug_tools.h hardware.h sched.h timer.h
vm.o: vm.c hardware.h sched.h boot.h disk.h drive.h vol.h mbr.h mount.h
vol.o: vol.c hardware.h debug_tools.h drive.h sched.h disk.h vol.h mbr.h \
 inode.h ifile.h


%.o: %.c
	$(CC) -o $@ $(CFLAGS) -c $< $(INCDIR)

###------------------------------
### Misc.
###------------------------------------------------------------
.PHONY: clean realclean depend
clean:
	$(RM) *.o $(addprefix bin/, $(BINARIES))
realclean: clean 
	$(RM) vdiskA.bin vdiskB.bin
depend : 
	$(CC) -MM $(INCDIR) *.c

