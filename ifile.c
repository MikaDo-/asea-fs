#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "hardware.h"

#include "drive.h"
#include "vol.h"
#include "mbr.h"
#include "inode.h"
#include "ifile.h"

extern mbr_str MBR;
extern unsigned int secSize;
extern superblock_str currentSuperblock;
extern unsigned int currentVol;
extern unsigned char buffer[512];

unsigned int create_ifile(enum file_type_e type){
    unsigned int nvBlock; 

    nvBlock = create_inode(type);
    if( nvBlock == 0)
        fprintf(stderr, "ERROR - __FILE__-__LINE__ : CANNOT CREATE IFILE\n");
	return nvBlock; 
}

int delete_ifile(unsigned int inumber){
    int err = 0;
    if( (err = delete_inode(inumber)) )
        fprintf(stderr, "ERROR - __FILE__-__LINE__ : CANNOT DELETE IFILE %u\n", inumber); 
    return err;
}

int open_ifile(file_desc_str *descriptor, unsigned int inumber){
    unsigned int firstBlock;
    int err = 0;
    inode_str inode; 
    if( (err = read_inode (inumber, &inode)) ){
        fprintf(stderr, "ERROR - __FILE__-__LINE__ : CANNOT READ INODE %u WHILE OPENING THE CORRESPONDING IFILE.\n", inumber); 
        return err;
    }
    descriptor->inumber = inumber;
    descriptor->size = inode.size;
    descriptor->pos = 0;
    descriptor->needsFlush = false;

    // on va chercher le premier bloc. S'il n'y en a pas encore, on remplit le buffer avec des zéros.
    firstBlock = vblock_of_fblock(inumber, 0, false);
    if (firstBlock == 0) 
        memset(descriptor->buffer, 0, secSize);
    else
        if( (err = read_bloc(currentVol, firstBlock, descriptor->buffer)) )
            fprintf(stderr, "ERROR - __FILE__-__LINE__ : CANNOT READ FIRST BLOCK %u OF IFILE %u\n", firstBlock, inumber); 

    return err;
}

int close_ifile(file_desc_str *descriptor){
    inode_str inode;
    int err = 0;
    
    // si on a des données en attente d'écriture, on flush sur le disque.
    flush_ifile(descriptor);
    
    //l'écriture du tampon a peut-être modifié la taille de fichier (notemment si le secteur correspondant n'était pas encore alloué !)
    if( (err = read_inode(descriptor->inumber, &inode)) ){
        fprintf(stderr, "ERROR - __FILE__-__LINE__ : CANNOT READ INODE %u WHILE OPENING THE CORRESPONDING IFILE.\n", descriptor->inumber); 
        return err;
    }
    inode.size = descriptor->size;
    if( (err = write_inode(descriptor->inumber, &inode)) )
        fprintf(stderr, "ERROR - __FILE__-__LINE__ : CANNOT WRITE INODE %u WHILE WRITING THE CORRESPONDING IFILE.\n", descriptor->inumber); 
    return err;
}

// retourne le block correspondant à l'octet byte du fichier
unsigned int block_of_byte(unsigned int byte){
    return byte / secSize;
}
// retourne l'octet du block correspondant à l'octet byte du fichier
unsigned int byte_in_block(unsigned int byte){
    return byte % secSize;
}

int flush_ifile(file_desc_str *descriptor){
    int err = 0;
    if (descriptor-> needsFlush) {
        unsigned int fbloc = block_of_byte(descriptor->pos);
		//printf("inumber : %u\n", descriptor->inumber);
        unsigned int vbloc = vblock_of_fblock(descriptor->inumber, fbloc, true);

        if( (err = write_bloc(currentVol, vbloc, descriptor->buffer)) )
            fprintf(stderr, "ERROR - __FILE__-__LINE__ : CANNOT WRITE vBLOCK %d WHILE FLUSHING INODE %u'S BUFFER. - err: %d\n", vbloc, descriptor->inumber, err); 
        else
            descriptor->needsFlush = false ; 
    }
    return err;
}

// relatif
int seek_ifile(file_desc_str *descriptor, int offset){
    int err = 0;
	
    unsigned int old_pos = descriptor->pos;
    unsigned int fbloc, vbloc; 
    
    descriptor->pos += offset;
    // si le bloc du byte auquel on se déplace est le même que celui actuel, on a fait le nécessaire en mettant l'offset à jour.
    if (block_of_byte(descriptor->pos) == block_of_byte(old_pos))
        return err;
        
    if( (err = flush_ifile(descriptor)) ){
        fprintf(stderr, "ERROR - __FILE__-__LINE__ : FLUSH OF vBLOCK %d OF IFILE %u FAILED WHILE SEEKING AR BYTE %u\n", vblock_of_fblock(descriptor->inumber, block_of_byte(old_pos), false),
                                                                                                                        descriptor->inumber,
                                                                                                                        descriptor->pos); 
        return err;
    }
    fbloc = block_of_byte(descriptor->pos);
    vbloc = vblock_of_fblock(descriptor->inumber, fbloc, false);

    // pareil que lors de l'ouverture de fichier, si le bloc retourné n'est pas encore alloué sur le disque, on remplit le tampon avec des zéros, et l'allocation se fera à l'écriture
    if (vbloc == 0)
        memset(descriptor->buffer, 0, secSize);
    else
        if( (err = read_bloc(currentVol, vbloc, descriptor->buffer)) )
            fprintf(stderr, "ERROR - __FILE__-__LINE__ : CANNOT READ vBLOCK %u OF IFILE %u WHILE SEEKING AR BYTE %u\n", vbloc, descriptor->inumber, descriptor->pos); 
        
    return err;
}
// absolu
int seek2_ifile(file_desc_str *descriptor, int offset){
    return seek_ifile(descriptor, offset - descriptor->pos);
}

int readc_ifile(file_desc_str *descriptor){
    if (descriptor->pos >= descriptor->size)
        return READ_EOF; 
    char byte;
    // on retourne le contenu du buffer
    byte = descriptor->buffer[byte_in_block(descriptor->pos)];
    seek_ifile(descriptor, 1);
    
    return byte; 
}
void printDescriptor(file_desc_str* desc){
	printf("File descriptor dump begin : \n");
		printf("pos  : %u \n", desc->pos);
		printf("inum : %u \n", desc->inumber);
		printf("size : %u \n", desc->size);
		printf("buff : %p \n", desc->buffer);
	printf("---- END ----\n");
}
// retourne pos du caractère écrit ou err
int writec_ifile(file_desc_str *descriptor, char c){
    unsigned int ibloc;
    int err = 0;
    /* write the char in the buffer */
    descriptor->buffer[byte_in_block(descriptor->pos)] = c;
    // Si c'est la première écriture, on s'assure que le bloc de destination sur le disque est correctement alloué.
    if (! descriptor->needsFlush) {
        if( (err = vblock_of_fblock(descriptor->inumber, block_of_byte(descriptor->pos), true)) <= 0){
            fprintf(stderr, "ERROR - __FILE__-__LINE__ : CANNOT ALLOCATE THE REQUIRED SPACE FOR WRITING FILE DESCRIPTOR %u\n", descriptor->inumber); 
            return err;
        }
        ibloc = err;
        descriptor->needsFlush = true; // dans tous les cas il faut écrire le tampon dans le secteur vu qu'on vient seulement de l'allouer.
    }
    
    // si on écrit le dernier byte du buffer (et donc qu'on va seeker au bloc suivant après cette écriture), on doit flush sur le disque. et charger le bloc suivant.
    if (byte_in_block(descriptor->pos) == secSize-1) {
        ibloc = vblock_of_fblock(descriptor->inumber, block_of_byte(descriptor->pos), false);
		printf("INFO - Changement de secteur pour les prochaines ecritures...\n");
        if( (err = write_bloc(currentVol, ibloc, descriptor->buffer)) ){
            fprintf(stderr, "ERROR - __FILE__-__LINE__ : CANNOT FLUSH FILE BUFFER ONTO THE DISK - err : %d\n", err); 
            return err;
        }
        
        ibloc = vblock_of_fblock(descriptor->inumber, block_of_byte(descriptor->pos+1), false);
        if (! ibloc) 
            memset(descriptor->buffer, 0, secSize);
        else
            read_bloc(currentVol, ibloc, descriptor->buffer);
        descriptor->needsFlush = false;
    }
    
    // MAj de la position du curseur.
    descriptor->pos++;
    if (descriptor->size < descriptor->pos)
        descriptor->size = descriptor->pos;
	//printf("writec pos : %u\n", descriptor->pos); 
    
    return descriptor->pos - 1; // position du caractère venant d'être écrit.
}
// retourne nombre d'octets lus
int read_ifile(file_desc_str *descriptor, void *buffer, unsigned int nbByte){
    unsigned int i;
    int c; 

    if (descriptor->pos >= descriptor->size) // cette vétification est déjà effectuée dans readc_ifile
        return READ_EOF; 

    for (i = 0; i < nbByte; i++) {
        if ((c = readc_ifile(descriptor)) == READ_EOF)
            return READ_EOF; 
        *((char *)buffer+i) = c; 
    }

    return i;
}
// retourne nombre d'octets écrits.
int write_ifile(file_desc_str *descriptor, const void *buffer, unsigned int nbByte){
    int err = 0;
    for (int i = 0; i < nbByte; i++)
        if ( (err = writec_ifile(descriptor, *((char *)buffer+i))) < 0){
            fprintf(stderr, "ERROR - __FILE__-__LINE__ : CANNOT WRITE IN iFILE %u - err: %d.\n", descriptor->inumber, err); 
            return err;
        }
    return nbByte;
}

