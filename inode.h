#ifndef INODE_H_INCLUDED
#define INODE_H_INCLUDED

#define INODE_INFO_SIZE	sizeof(inode_str)
#define INDx_CAPACITY   64
#define MAX_FILE_SIZE   12+INDx_CAPACITY+INDx_CAPACITY*INDx_CAPACITY+INDx_CAPACITY*INDx_CAPACITY*INDx_CAPACITY

typedef enum bool_e{
    true = 1, false = 0
} bool;


typedef enum file_type_e{
    FT_FILE, FT_DIRECTORY, FT_UNKNOWN
} file_type_e;


// si le type est fichier, alors les blocs référencés contiennent des données
// si le type est répertoire, alros les blocs référencés contiennent des entrées de répertoires contenant un nom de fichier et un numéro d'inode
typedef struct inode_str{
	file_type_e     type;
	unsigned int    size;
	unsigned int    refCount;           // on sait ainsi s'il faut supprimer ou non l'inode lorsqu'on supprime (son)/(un de ses) répertoire/s parent/s
	unsigned int   	direct_blocks[12];
	unsigned int    ind1_block;
	unsigned int	ind2_block;
	unsigned int	ind3_block;
} inode_str;

typedef struct indx_table_str{
	unsigned int    inumbers[INDx_CAPACITY];
} indx_table_str;

int initTestIndx();
int testAllocationDataBlocks();
int print_inode(unsigned int inumber);
int read_inode(unsigned int inumber, inode_str *inode);
int write_inode(unsigned int inumber, inode_str *inode);
int create_inode(file_type_e type);
int delete_inode(unsigned int inumber);
int read_indx_table(unsigned int inumber, indx_table_str *ind_table);
int write_indx_table(unsigned int inumber, indx_table_str *ind_table);
int create_indx_table();
int del_ind1_table(unsigned int inumber);
int del_ind2_table(unsigned int inumber);
int del_ind3_table(unsigned int inumber);
int resolve_ind1(unsigned int inumber, unsigned int fblock, bool do_allocate);
int resolve_ind2(unsigned int inumber, unsigned int fblock, bool do_allocate);
int resolve_ind3(unsigned int inumber, unsigned int fblock, bool do_allocate);
int vblock_of_fblock(unsigned int inumber, unsigned int fblock, bool do_allocate);

#endif
