#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "hardware.h"
#include "drive.h"
#include "vol.h"
#include "mbr.h"
#include "inode.h"
#include "ifile.h"
#include "mount.h"

int cfile(unsigned int source_inumber){
    file_desc_str source_fd, dest_fd;
    unsigned int dest_inumber;
    int err = 0;
    
    if( (err = create_ifile(FT_FILE)) <= 0 ){
		fprintf(stderr, "ERROR - cfile : CANNOT CREATE DESTINATION FILE. - err: %d\n", err);
        return err;
    }
    dest_inumber = err;
    printf("%u\n", dest_inumber);

    if( (err = open_ifile(&dest_fd, dest_inumber)) ){
		fprintf(stderr, "ERROR - cfile : CANNOT OPEN DESTINATION FILE. - err: %d\n", err);
        return err;
    }
    if( (err = open_ifile(&source_fd, source_inumber)) ){
		fprintf(stderr, "ERROR - cfile : CANNOT OPEN SOURCE FILE. - err: %d\n", err);
        return err;
    }

    unsigned char byte;
    while((byte = readc_ifile(&source_fd)) != READ_EOF)
        writec_ifile(&dest_fd, byte);

    close_ifile(&dest_fd);
    close_ifile(&source_fd);
}

static void usage(const char *prgm)
{
    fprintf(stderr, "[%s] usage:\n\t"
            "%s inumber\n", prgm, prgm);
    exit(EXIT_FAILURE);
}

int main (int argc, char *argv[]){
    unsigned inumber;
    
    if (argc != 2)
        usage(argv[0]);

    errno = 0;
    inumber = strtol(argv[1], NULL, 10);
    if (errno || inumber <= 0)
        usage(argv[0]);

    mount();
    cfile(inumber);
    umount();
    
    exit(EXIT_SUCCESS);         
}
