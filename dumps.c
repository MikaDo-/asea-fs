#include "stdio.h"
#include "stdlib.h"
#include "ctype.h"

#include "hardware.h"
#include "drive.h"
#include "mount.h"

void dumps(unsigned int cyl, unsigned int sec){
	mSeek(cyl, sec);

	_out(HDA_DATAREGS,   0x00); // le nb de secteurs est sur 16 bits, donc 2 _out dont un à 0.
	_out(HDA_DATAREGS+1, 0x01);
	_out(HDA_CMDREG,  	 CMD_READ);
	_sleep(HDA_IRQ);
	unsigned int tailleSec;

	_out(HDA_CMDREG, 	CMD_DSKINFO);
	tailleSec  = _in(HDA_DATAREGS+4) << 8;
	tailleSec |= _in(HDA_DATAREGS+5) & 0xff;

	printf("sector(%d,%d) -- taille %d --: \n", cyl, sec, tailleSec); 
    for(int i = 0;i<tailleSec;i+=16){
		for(int j = i;j<i+16;j++){
			printf("%02x",  *(MASTERBUFFER + j));
			if(j%2)
				printf(" ");
		}
		for(int j = i;j<i+16;j++){
			if(isprint(*(MASTERBUFFER + j)))
				printf("%c",  *(MASTERBUFFER + j));
			else
				printf(".");
			if(i%2)
				printf(" ");
		}
		printf("\n");
	}
	printf("\n");
}

int main(int argc, char* argv[]){
	initConfig();
	initDisk();
	printf("Outil dmps - ASE TP6\n====================\n						Alexis Beaujet M1\n\n\n");
	unsigned int cyl, sect;
	printf("Cylindre : ");
	scanf("%u", &cyl);
	printf("\nSecteur : ");
	scanf("%u", &sect);
	printf("\n\nContenu du secteur (%u,%u) : \n", cyl, sect);
	dumps(cyl, sect);
	printf("\n\n");

	return EXIT_SUCCESS;
}
