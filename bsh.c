#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <assert.h>
#include <signal.h>
#include <readline/readline.h>

#include "hardware.h"
#include "mount.h"
#include "inode.h"
#include "ifile.h"
#include "dir.h"
#include "sched.h"
#include "boot.h"
#include "vol.h"
#include "debug_tools.h"

/*
 * Alors alors... C'est parti !
 *
 * Shell de base - ASE{a?} 2015-2016 - Alexis Beaujet M1
 *
 * La liste des fonctions disponibles est quelques
 * lignes en dessous dans le code, pas besoin de s'étaler.
 * 
 * Maintenant ce qui marche (et là ça vaut le coup) :
 *     - cd
 *     - ls
 *     - mkdir
 *     - touch
 *     - write
 *     - cat
 *     - cp
 *     - rm
 *     - ping
 *     - pong
 *     - compute
 *     - top
 *     - &
 *
 * Le recouvrement du disque est assuré dans un premier
 * temps à l'aide d'un automate, puis dans une autre version
 * plus simplement à l'aide d'un sémaphore.
 * Je ferai sans doute deux cibles de compilation dans le Makefile,
 * une pour chaque méthode.
 */

char currentDir[200] = {'\0'};
inode_str currentInode;

extern unsigned int currentVol;
extern unsigned int secSize;
extern char sched_running;
extern ctx_s* ctx_current;


#define ARG_MAX_SIZE 200 // taille max d'un seul argument

/* ------------------------------
   command list
   ------------------------------------------------------------*/
struct _cmd {
    char *name;
    void (*fun) (struct _cmd *c);
    char *comment;
	char **arguments; // tableaux de chars. Contient les arguments séparés d'espaces lors de l'appel de la commande.
};

static void _test(struct _cmd *c);

static void _cd(struct _cmd *c);
static void _mkdir(struct _cmd *c);
static void _pwd(struct _cmd *c);
static void _ls(struct _cmd *c);
static void _rm(struct _cmd *c);
static void _mv(struct _cmd *c);
static void _cp(struct _cmd *c) ;
static void _touch(struct _cmd *c);
static void _write(struct _cmd *c);
static void _cat(struct _cmd *c);
static void _append(struct _cmd *c);
static void _dumps(struct _cmd *c);
static void ping(struct _cmd *c);
static void pong(struct _cmd *c);
static void compute(struct _cmd *c);
static void top(struct _cmd *c);
static void _kill(struct _cmd *c);
static void mkfs(struct _cmd *c);
static void xit(struct _cmd *c);
static void none(struct _cmd *c);
static void help(struct _cmd *c);

static struct _cmd commands [] = {
    {"cd",	   _cd,		"\tUsage : cd dir\n\tDescription : Change le répertoire courant vers dir.", NULL},
    {"mkdir",  _mkdir,	"\tUsage : mkdir dir\n\tDescription : Crée un répertoire dir a l'interieur du repertoire courant.", NULL},

    {"test",   _test,	"\tUsage : cd dir\n\tDescription : Change le répertoire courant vers dir.", NULL}, // pour tester l'interpreteur

    {"pwd",	   _pwd, 	"\tUsage ; pwd\n\tDescription : Affiche le chemin du repertoire courant.", NULL},
    {"ls",	   _ls, 	"\tUsage : ls\n\tDescription : Liste le contenu du repertoire courant.", NULL},
    {"rm",     _rm,		"\tUsage : rm file1 [file2 file3...]\n\tDescription : Supprime les fichiers donnes en parametres.", NULL},
    {"mv",     _mv,		"\tUsage : mv src dest\n\tDescription : Deplace src dans dest.", NULL},
    {"cp", 	   _cp,		"\tUsage : cp src dest\n\tDescription : Copie src dans destination.", NULL},
    {"touch",  _touch,	"\tUsage : touch file1 [file2 file3...] \n\tDescription : Cree un/des fichiers vides nommes file1, file2, file3...", NULL},
    {"write",  _write,	"\tUsage : write filename\n\tDescription : Entre en mode saisie pour le contenu du fichier nomme filename", NULL},
    {"cat",    _cat,	"\tUsage : cat filename\n\tDescription : Affiche le contenu du fichier nomme filename", NULL},
    {"append", _append,	"\tUsage : append filename content of target file\n\tDescription : Ecrit \"Content of the target file\" a la fin du fichier nomme filename", NULL},
    {"dumps",  _dumps,	"\tUsage : dumps blockId\n\tDescription : Ecrit le contenu du bloc blockId sur la sortie standard.", NULL},
    {"ping",   ping,	"\tUsage : ping\n\tDescription : Ecrit ping en boucle sur la sortie standard.", NULL},
    {"pong",   pong,	"\tUsage : pong\n\tDescription : Ecrit pong en boucle sur la sortie standard.", NULL},
    {"compute",compute,	"\tUsage : compute\n\tDescription : Effectue un calcul à l'infini dans le but de consommer du temps CPU.", NULL},
    {"top",    top,		"\tUsage : top\n\tDescription : Affiche la liste des taches en cours ainsi que leur utilisation CPU et leur etat.", NULL},
    {"kill",   _kill,	"\tUsage : kill pid\n\tDescription : Tue le contexte pid.", NULL},
    {"mkfs",   mkfs,	"\tUsage : mkfs volId\n\tDescription : Initialise un systeme de fichiers dans le volume volId.", NULL},
    {"help",   help,	"\tUsage : help\n\tDescription : Affiche cette aide", NULL},
    {"exit",   xit,	    "\tUsage : exit\n\tDescription : Quitte le shell.", NULL},
    {0,        none, 	"Commande inconnue. Tentez : help", NULL}
} ;

static void _test(struct _cmd *c){
	char** cur = c->arguments;
	while(*cur != NULL){
		printf("%s\n", *cur);
		cur++;
	}
}

static char deletePathElement(char** pathArray, int index){
	int i;
	free(pathArray[index]);
	for(i =index;pathArray[i+1]!=NULL;i++)
		pathArray[i] = pathArray[i+1];
	pathArray[i] = NULL;
	return 0;
}

/*
 * Le but de cette fonction est de transformer un chemin éventuellement constitué d'éléments relatives ".."/"."
 * en un chemin absolu utilisable directement dans inumber_of_path.
 *  1. Chemin absolu ou relatif ? (commence par '/' ou non)
 *      1. absolu :
 *         on copie le chemin dans fullpath
 *      2.  relatif :
 *         on concatène currentDir, "/", et le chemin. Le tout est stocké dans fullpath.
 *  2. Trailin slash ou non ? (path[strlen-1] == '/')
 *      1. oui :
 *         path[strlen-1] = '\0'
 *  3. On découpe le chemin en unités séparées par les /
 *      1. déterminer la profondeur du chemin (nombre de '/') : depth
 *      2. on alloue un tableau de 'depth+1' chaînes de caratères de 50 caractères chacune : pathArray
 *      3. on parcourt le chemin en copiant le contenu dans les cases de pathArray. A chaque '/' rencontré, on change de case.
 *  4. On parcourt pathArray
 *      1. si une case vaut "..", alors on la supprime ainsi que la case qui la précède (si possible), et on décrémente depth du nombre de cases supprimées.
 *      2. si une case vaut ".", alors on la supprime sans oublier de décrémenter depth.
 *  5. On recolle les unités de pathArray
 *      1. on remplace le contenu de fullpath par un unique '/'
 *      2. pour chaque case de pathArray:
 *      	1. on concatène un '/' à fullpath (sauf si première case)
 *          2. on concatène le contenu de la case à fullpath
 *      3. on libère pathArray ainsi que les cases le constituant
*/
static char *expandPath(const char* path){
	char *fullpath = calloc(BASENAME_MAX_SIZE, 1);
	// 1. Chemin relatif ou absolu
	if(path[0] != '/'){
		// 1.2
		strcpy(fullpath, currentDir);
		if(strcmp(fullpath, "/"))
			strcat(fullpath, "/");
	}
	strcat(fullpath, path);
	// 2. Trailing slash ou non ?
	int pathSize = strlen(path);
	if(pathSize > 1 && fullpath[pathSize-1] == '/'){
		fullpath[pathSize-1] = '\0';
		pathSize--;
	}
	
	// 3. Découpage du chemin
	char depth = 0, *cur = fullpath;
	// 3.1 Déterminer la profoncdeu du chemin
	for(;*cur!='\0';cur++)
		if(*cur == '/')
			depth++;
	// 3.2 Allocation du tableau de chaînes pour stocker les éléments de chemin
	char** pathArray = (char**)calloc((depth+1), sizeof(char*));
	int i, j;
	cur = fullpath;
	// 3.3 On copie fullpath dans les cases de pathArray en changeant de case à chaque '/' recontré
	for(i = 0;i<depth;i++){
		pathArray[i] = (char*)calloc(50, 1);
		while(*cur && *cur=='/') // il se peut qu'on passe plusieurs '/' et donc qu'on ait alloué plus de cases que nécessaire. Mais pas grave puisqu'on a utilisé calloc au dessus.
			cur++;
		for(j=0; *cur != '\0' && *cur!='/';j++)
			pathArray[i][j] = *cur++;
		pathArray[i][j] = '\0';
	}
	
	// 4. On parcourt pathArray. Pour chaque case :
	//    si contenu == ..
	//		on supprime l'élément courrant ainsi que le précédent si possible, depth-=nb_cases_supprimées
	//	  si contenu == .
	//		on supprime l'élément courant seul. depth--
	for(int i =0;i<depth;i++){
start_path_processing: // utile pour retourner au début du traitement sans incrémenter i
		if(i>=depth)
			break;
		if(strcmp(pathArray[i], "..") == 0){	// si:   |/|nv1|/|nv2.1|/|..|/|nv2.2
			deletePathElement(pathArray, i);	// i:					  xx
			if(i > 0){
				deletePathElement(pathArray, i-1);
				i-=1; // on retourne en arrière et on prévoir l'incré
				depth--;
			}
			depth--;
			goto start_path_processing;
		}
		if(strcmp(pathArray[i], ".") == 0){
			deletePathElement(pathArray, i);
			depth--;
			goto start_path_processing;
		}
	}

	//for(int i = 0;i<depth;i++)
	//	printf("     patharray[%2d] = %s\n", i, pathArray[i]);
	// 5. On recolle les morceaux...
	strcpy(fullpath, "/");
	for(int i = 0;i<depth;i++){
		if(i>0)
			strcat(fullpath, "/");
		strcat(fullpath, pathArray[i]);
	}
	for(i=0;pathArray[i]!=NULL;i++)
		free(pathArray[i]);
	free(pathArray);

	return fullpath;
}

/*
 * Changement de répertoire courant :

 *  1. On récupère le numéro d'inode correspondant à fullpath : inumber
 *  2. inumber est-il valide ?
 *  3. On charge l'inode inumber. Est-il bien un répertoire ?
 *      1. oui :
 *         on charge l'inode correspondant à inumber dans currentInode.
 *         on copie fullpath dans currentDir.
 *         RET 0
 *      2. non :
 *         message d'erreur
 *         RET fail
 */
static char cd(const char* path){
	if(path == NULL){
		fprintf(stderr, "ERROR - %s:%d - CD : YOU MUST SUPPLY A DIRECTORY PATH. NULL POINTER GIVEN.\n", __FILE__, __LINE__);
		return -20;
	}
	int err = 0;
	
	char* fullpath = expandPath(path);
	// 1. On récupère le numéro de l'inode correspondant à fullpath
	unsigned int inumber = inumber_of_path(fullpath);
	inode_str 	 inodeTmp;
	// 2. inumber est-il valide ?
	if(inumber < 1){
		fprintf(stderr, "ERROR - %s:%d - CD : INVALID PATH GIVEN AS INPUT. CANNOT FIND CORREPONDING INUMBER FOR THIS PATH \n    --->fullpath: %s\n    --->inumber: %u\n", __FILE__, __LINE__, fullpath, inumber);
		free(fullpath);
		return -21;
	}
	// 3. On charge l'inode inumber.
	if( (err = read_inode(inumber, &inodeTmp)) ){
		fprintf(stderr, "ERROR - %s:%d - COULD NOT READ INODE %u WHEN SWITCHING CURRENT DIRECTORY. err : %d\n", __FILE__, __LINE__, inumber, err);
		free(fullpath);
		return -22;
	}
	// 3.1 On switch sur ce répertoire
	if(inodeTmp.type == FT_DIRECTORY){
		strcpy(currentDir, fullpath);
		currentInode = inodeTmp;
		free(fullpath);
		return 0;
	}
	// 3.2 Cet inode n'est pas un répertoire. On chiale avec une petit message d'erreur.
	fprintf(stderr, "WARNING - %s:%d - CANNOT SWITCH DIRECTORY TO A REGULAR FILE.\n", __FILE__, __LINE__);
	free(fullpath);
	return -23;
}

static void _cd(struct _cmd *c){
	cd(c->arguments[0]);
}

static char mkdir(const char* dirname){
	if(dirname == NULL || strlen(dirname) < 1){
		fprintf(stderr, "ERROR - %s:%d - MKDIR : YOU MUST SUPPLY A VALID DIRECTORY NAME. NONE GIVEN\n", __FILE__, __LINE__);
		return -28;
	}
	if(!strcmp(dirname, "..") || !strcmp(dirname, ".")){
		fprintf(stderr, "ERROR - %s:%d - MKDIR : FILENAMES \"..\" AND \".\" ARE RESERVED.\n", __FILE__, __LINE__);
		return -31;
	}
	for(int i = 0;dirname[i] != '\0';i++)
		if(dirname[i] == '/'){
			fprintf(stderr, "ERROR - %s:%d - MKDIR : FOUND A '/' IN THE DIRECTORY NAME. NOT SUPPRORTED YET.\n", __FILE__, __LINE__);
			return -36;
		}
	
	unsigned int currentInumber;
	if( (currentInumber = inumber_of_path(currentDir)) < 1){
		fprintf(stderr, "ERROR - %s:%d - MKDIR : CANNOT DETERIMNE CURRENT DIRECTORY INUMBER.\n", __FILE__, __LINE__);
		return -30;
	}
	if( inumber_of_basename(currentInumber, dirname) > 0){
		fprintf(stderr, "ERROR - %s:%d - MKDIR : DIRECTORY %s ALREADY EXISTS.\n", __FILE__, __LINE__, dirname);
		return -37;
	}

	unsigned int dir = create_ifile(FT_DIRECTORY);
	if(!dir){
		fprintf(stderr, "ERROR - %s:%d - MKDIR : CANNOT CREATE A NEW DIRECTORY-TYPE INODE.\n", __FILE__, __LINE__);
		return -29;
	}
	
	return add_entry(currentInumber, dir, dirname);
}

static void _mkdir(struct _cmd *c){
	mkdir(c->arguments[0]);
}


static char pwd(){
	printf("%s", currentDir);
	return 0;
}

static void _pwd(struct _cmd *c){
	pwd();
	printf("\n");
}

static char ls(const char* path){
	unsigned int inumber;
	if( (inumber = inumber_of_path(path)) > 0)
		return display_entries(inumber);
	else
		return -22;
}

static char rm_inode(const unsigned int inumber){
	int err = 0;
	//parcours des feuilles	
    inode_str inode;
    if( (err = read_inode(inumber, &inode)) ){
        _printf_err("CANNOT READ DIRECTORY INODE %u.\n", inumber);
        return err;
    }

	// SI c'est un fichier :
	if(inode.type == FT_FILE || inode.size == 0){
		if( (err = delete_ifile(inumber)) )
			_printf_err("RM : CANNOT DELETE IFILE %u. err : %d", inumber, err);
		return err;
	}
	// si c'est un répertoire :
    unsigned int entryCount = inode.size / sizeof(entry_str);

	_dprintf("RM : entryCount : %u, inode.size : %u\n", entryCount, inode.size);
    entry_str entry;
    file_desc_str desc;
    if( (err = open_ifile(&desc, inumber)) ){
        _printf_err("RM CANNOT OPEN DIR FILE %u. err : %d\n", inumber, err);
        return err;
    }
    for(int i = 0; i < entryCount; i++){
		if(entry.inumber == 0)
			continue; // l'entrée est déjà supprimée...
        read_ifile(&desc, &entry, sizeof(entry_str));
        printf("DELETING    %30s  |  %4u  |  ", entry.basename, entry.inumber);
		// on détermine le type de l'inode référencé
		inode_str inodeTmp;
		if( (err = read_inode(entry.inumber, &inodeTmp)) > 0){
			_printf_err("RM CANNOT DETERMINE INODE TYPE FOR ENTRY %u\n", entry.inumber);
			err = 0;
			continue;
		}
		switch(inodeTmp.type){
			case FT_DIRECTORY:
				//rapel de rm_inode
				printf("DIR\n");
				if( (err = rm_inode(entry.inumber)) ){ // <-- RECUR
					_printf_err("RM CANNOT REMOVE DIRECTORY INODE %u WHILE DELETING ITS PARENT DIRECTORY. err : %d\n", entry.inumber, err);
					return -25;
				}
				break;
			case FT_FILE:
				if( (err = delete_ifile(entry.inumber)) ){
					_printf_err("RM CANNOT REMOVE FILE INODE %u WHILE DELETING ITS PARENT DIRECTORY. err : %d\n", entry.inumber, err);
					return -26;
				}
				printf("FILE\n");
				break;
			default:
				printf("UNKNOWN\n");
				continue;
		}
		if( (err = del_entry(inumber, entry.basename)) <= 0 ){ // taille écrite/supprimee sur le disque
			_printf_err("RM : CANNOT DELETE DIRECTORY ENTRY. basename = %s\n", entry.basename);
			return -41;
		}
		err = 0;
    }
	// Ben oui c'est pas le tout de supprimer le contenu du répertoire, il faut aussi supprimer le répertoire lui même... Remarqué en voyant certains blocs non libérés dans le ls (inumbers)
	if( (err = delete_ifile(inumber)) )
		_printf_err("RM : CANNOT DELETE IFILE %u. err : %d\n", inumber, err);
	return err;
}

static char rm(const char* path){
	if(path == NULL || strlen(path) < 1){
		_print_err("RM : YOU MUST SUPPLY A VALID  PATH. NULL POINTER GIVEN.\n");
		return -24;
	}
	if(!strcmp(path, "..") || !strcmp(path, ".")){
		_print_err("RM : FILENAMES \"..\" AND \".\" ARE RESERVED.\n");
		return -31;
	}
	if(!strcmp(path, "/")){
		_print_err("RM : DELETING ROOT DIRECTORY IS FORBIDDEN.\n");
		return -31;
	}
	for(int i = 0;path[i] != '\0';i++)
		if(path[i] == '/'){
			_print_err("RM : FOUND A '/' IN THE DIRECTORY NAME. NOT SUPPRORTED YET.\n");
			return -36;
		}

	int err = 0;
	unsigned int inumber;
	
	unsigned int currentInumber;
	if( (currentInumber = inumber_of_path(currentDir)) < 1){
		_print_err("MKDIR : CANNOT DETERIMNE CURRENT DIRECTORY INUMBER.\n");
		return -30;
	}

	if(strcmp(path, "*")){ // si on veut supprimer un fichier spécifique, et non pas *
		if( (inumber = inumber_of_basename(currentInumber, path)) == 0){
			_printf_err("RM : NO SUCH FILE OR DIRECTORY IN CURRENT DIR. path: %s.\n", path);
			return -39;
		}
		if( (err = rm_inode(inumber)) ){
			_printf_err("RM : CANNOT DELETE FILE/DIRECTORY INODE. err=%d\n", err);
			return err;
		}
		if( (err = del_entry(currentInumber, path))  <= 0){ //taille écrite/supprimmee sur le disque
			_printf_err("RM : CANNOT DELETE DIRECTORY ENTRY. basename = %s\n", path);
			return -40;
		}
		err = 0;
	}else{
		fileinfo_str* files;
		unsigned int entryCount;
		getDirContents(currentInumber, &files, &entryCount);
		for(int i = 0;i<entryCount;i++)
			if(files[i].inumber != 0){
				printf("DEBUG - deleting file %s", files[i].basename);
				err = rm(files[i].basename);
				printf("\t\t %d\n", err);
			}
	}
	return err;
}

static void _rm(struct _cmd *c){
	int err = 0;
	char** chemin;
	chemin = c->arguments;
	unsigned int i=0;
	for(;*chemin!=NULL;chemin++){
		_dprintf("RM DELETING %s\n", *chemin);
		err = rm(*chemin);
		i++;
	}
	if(!i)
		_print_warn("RM : YOU MUST SUPPLY AT LEAST ONE TARGET FILE. NONE GIVEN\n");
	if(err)
		_print_warn("RM : There has been errors while deleting one or more of the specified targets.\n");
    printf("rm returned %d\n", err);
}


static void _ls(struct _cmd *c){
	ls(currentDir);
    printf("\n");
}

static void _mv(struct _cmd *c){
    printf("\n");
}

static const char* basename(const char* fullpath){
	unsigned int length = strlen(fullpath);
	unsigned int pos = 0;
	for(int i = 0;i<length;i++)
		if(fullpath[i] == '/' && fullpath[i+1] != '\0')
			pos = i+1;
	return fullpath+pos;

}

//            /123/4567/890
//  basename            |       (i=10)
//  fullpath  |                  (j=0)
//  dirname   |<     >|      (l=i-j-1)
static char* dirname(const char* fullpath){
	char* res = (char*)calloc(strlen(fullpath), 1);
	strncpy(res, fullpath, basename(fullpath)-fullpath-1);
	if(!strcmp(res, "")) // putain de cas particulier à la con
		sprintf(res, "/");
	return res;
}

// on doit pouvoir copier un fichier d'un répertoire dans un d'un autre répertoire...
// il faut donc expand les deux noms de fichiers.
static char cp(const char* source, const char* dest){
	char err = 0;
	if(source == NULL || strlen(source) < 1 ||
	   dest   == NULL || strlen(dest) < 1){
		fprintf(stderr, "ERROR - %s:%d - CP: YOU MUST SUPPLY VALID FILENAMES. ONE OR MORE MISSING.\n", __FILE__, __LINE__);
		return -32;
	}
	if(!strcmp(source, "..") || !strcmp(source, ".") ||
	   !strcmp(dest, "..")   || !strcmp(dest, ".")){
		fprintf(stderr, "ERROR - %s:%d - CP : ONE OR MORE FILENAMES CONTAIN(S) \"..\" AND/OR \".\". NYI.\n", __FILE__, __LINE__);
		return -33;
	}
	char* full_source = expandPath(source);
	char* full_dest = expandPath(dest);

	unsigned int inbSource, inbDest;
	if( (inbSource = inumber_of_path(full_source)) < 1){
		fprintf(stderr, "ERROR - %s:%d - CP : NO SUCH FILE OR DIRECTORY FOR SOOURCE FILENAME %s\n", __FILE__, __LINE__, source);
		return -42;
	}
	if( (inbDest = inumber_of_path(full_dest)) < 1){
		if( (inbDest = create_ifile(FT_FILE)) < 1)
			return -43;
		unsigned int currentInumber;
		char *target_dirname = dirname(full_dest);
		printf("DEBUG - target_dirname : %s\n", target_dirname);
		if( (currentInumber = inumber_of_path(target_dirname)) < 1){
			fprintf(stderr, "ERROR - %s:%d - CP : CANNOT DETERIMNE CURRENT DIRECTORY INUMBER.\n", __FILE__, __LINE__);
			return -35;
		}
		add_entry(currentInumber, inbDest, basename(dest));
	}

	file_desc_str fd_source, fd_dest;
	open_ifile(&fd_source, inbSource);
	open_ifile(&fd_dest, inbDest);

	int car;
	while( (car = readc_ifile(&fd_source)) != READ_EOF){
		if(car < 0){
			fprintf(stderr, "WARNING - %s:%d - CP : CANNOT GET NEXT CHARACTER. pos=%u - err=%d\n", __FILE__, __LINE__, fd_source.pos, car);
			continue;
		}
		if(writec_ifile(&fd_dest, car) < 0){
			fprintf(stderr, "WARNING - %s:%d - CP : CANNOT WRITE NEXT CHARACTER. pos=%u - err=%d\n", __FILE__, __LINE__, fd_dest.pos, car);
			continue;
		}
	}
	if((err=close_ifile(&fd_source))||(err=close_ifile(&fd_dest)))
		fprintf(stderr, "WARNING - %s:%d - CAT : CANNOT CLOSE ONE OF THE FILE PROPERLY.\n", __FILE__, __LINE__);
	return err;
}

static void _cp(struct _cmd *c){
	if(c->arguments[0] != NULL && c->arguments[1] != NULL)
		printf("cp returned : %d\n", cp(c->arguments[0], c->arguments[1]));
	else
		fprintf(stderr, "YOU DUMBASS\n");
}

static char touch(const char* filename){
	if(filename == NULL || strlen(filename) < 1){
		fprintf(stderr, "ERROR - %s:%d - TOUCH: YOU MUST SUPPLY A VALID FILENAME. NONE GIVEN\n", __FILE__, __LINE__);
		return -32;
	}
	if(!strcmp(filename, "..") || !strcmp(filename, ".")){
		fprintf(stderr, "ERROR - %s:%d - TOUCH : FILENAMES \"..\" AND \".\" ARE RESERVED.\n", __FILE__, __LINE__);
		return -33;
	}
	for(int i = 0;filename[i] != '\0';i++)
		if(filename[i] == '/'){
			fprintf(stderr, "ERROR - %s:%d - TOUCH : FOUND A '/' IN THE FILENAME. NOT SUPPRORTED YET.\n", __FILE__, __LINE__);
			return -36;
		}
	
	unsigned int currentInumber;
	if( (currentInumber = inumber_of_path(currentDir)) < 1){
		fprintf(stderr, "ERROR - %s:%d - TOUCH : CANNOT DETERIMNE CURRENT DIRECTORY INUMBER.\n", __FILE__, __LINE__);
		return -35;
	}
	if( inumber_of_basename(currentInumber, filename) > 0){
		fprintf(stderr, "ERROR - %s:%d - TOUCH : FILE %s ALREADY EXISTS.\n", __FILE__, __LINE__, filename);
		return -37;
	}
	
	unsigned int inumber = create_ifile(FT_FILE);
	if(!inumber){
		fprintf(stderr, "ERROR - %s:%d - TOUCH : CANNOT CREATE A NEW FILE INODE.\n", __FILE__, __LINE__);
		return -34;
	}
	
	return add_entry(currentInumber, inumber, filename);
    printf("\n");
}

static void _touch(struct _cmd *c){
	char ret;
	for(int i=0;c->arguments[i] != NULL;i++)
		if( (ret = touch(c->arguments[i])) )
			break;
    printf("touch returned : %d\n", ret);
}

int append(char * filename, char** args){
    file_desc_str fd;
    unsigned int current_inumber;
    unsigned int inumber;
    int err = 0;
	char content[200]={'\0'};

    char filename2[100];
    strcpy(filename2, filename);
	
	//si le chemin existe deja on ajoute au fichier existant
	//strncpy(currentDir,currentDir,1);
	printf("filename:%s\n",filename); 
	printf("currentDir:%s\n",currentDir); 
	sprintf(filename,"%s%s",currentDir,filename2);
	printf("filename:%s\n",filename); 
	if( (current_inumber = inumber_of_path(filename)) < 1){
		fprintf(stderr, "ERROR - %s:%d - WRITE : CANNOT DETERIMNE CURRENT DIRECTORY INUMBER.\n", __FILE__, __LINE__);
		return -44 ;
	}
	printf("Inumber du dossier : %u\n", current_inumber);
	if((inumber = inumber_of_path(filename)) == 0 ){
        // le fichier n'existe pas, on le cree*/
        if( (err = create_ifile(FT_FILE)) <= 0 ){
            fprintf(stderr, "ERROR - append : CANNOT CREATE DESTINATION FILE. - err: %d\n", err);
            return err;
        }
        inumber = (unsigned int)err;
        printf("Inumber du fichier cree : %u\n", inumber);

    }else{
        inumber = inumber_of_basename(current_inumber, filename);
        printf("Inumber du fichier a ouvrir : %u\n", inumber);
	}
	

	//on range les arguments dans une chaine de caractères
	sprintf(content,"%s",args[1]);
	for(int i=2; args[i] != NULL;i++)
		sprintf(content,"%s %s",content,args[i]);
	printf("DEBUG - WRITE : content : %s \n", content);	
    
	//on ouvre le fichier
    if( (err = open_ifile(&fd, inumber)) ){
		fprintf(stderr, "ERROR - append : CANNOT OPEN DESTINATION FILE. - err: %d\n", err);
        return err;
    }
	
	int len_content= strlen(content);
	printf("strlen: %d ",len_content); 
	for(int i=0; i<len_content; i++){
        if( (err = writec_ifile(&fd,content[i])) < 0 ){
            fprintf(stderr, "ERROR - APPEND : CANNOT WRITE INTO DESTINATION FILE. - err: %d\n", err);
            return err;
        }
	}

    if( (err = close_ifile(&fd)) ){
        fprintf(stderr, "ERROR - nfile : CANNOT CLOSE DESTINATION FILE. - err: %d\n", err);
        return err;
    }
        
    return 0;
}

/*
 * 1. on vérifie si le fichier destination existe déjà ou pas.
 *     1.1 déterminer l'inumber du répertoire courant.
 *     1.2 vérifier si le fichier se trouve dans le répertoire.
 *         oui:
 *           1.2.1 on averti en demandant confirmation
 *           1.2.2  on supprime le fichier.
 * 2. on crée un nouveau fichier.
 * 3. on l'ajoute à l'index du répertoire courant.
 * 4. on l'ouvre
 * 5. on écrit dedans
 * 6. on le ferme.
 */


int write(const char * name){
    file_desc_str fd;
    unsigned int inumber, currentInumber;
    int err = 0, c;
// 1.
	// 1.1
	if( (currentInumber = inumber_of_path(currentDir)) < 1){
		fprintf(stderr, "ERROR - %s:%d - WRITE : CANNOT DETERIMNE CURRENT DIRECTORY INUMBER. err : %d\n", __FILE__, __LINE__, currentInumber);
		return -42;
	}
	// 1.2
	if((inumber = inumber_of_basename(currentInumber, name) > 0)){
		fprintf(stderr, "WARNING - %s:%d - WRITE : FILE %s ALREADY EXISTS: REWRITING FILE.\n", __FILE__, __LINE__, name);
		rm(name);
	}
// 2.
	if( (err = create_ifile(FT_FILE)) <= 0 ){
		fprintf(stderr, "ERROR - nfile : CANNOT CREATE DESTINATION FILE. - err: %d\n", err);
		return err;
	}
	inumber = err;
// 3.
	printf("Ajout de l'entree \"%s\" dans le repertoire \"%s\"\n", name, currentDir);
    if( (err = add_entry(currentInumber, inumber, name)) )
		return err;

	err = 0;
    printf("DEBUG - write : Inumber du fichier a editer : %u\n", inumber);
// 4.
    if( (err = open_ifile(&fd, inumber)) ){
		fprintf(stderr, "ERROR - nfile : CANNOT OPEN DESTINATION FILE. - err: %d\n", err);
        return err;
    }
// 5.
    while((c=getchar()) != EOF)
        if( (err = writec_ifile(&fd, c)) < 0 ){
            fprintf(stderr, "ERROR - nfile : CANNOT WRITE INTO DESTINATION FILE. - err: %d\n", err);
            return err;
        }
// 6.
    if( (err = close_ifile(&fd)) ){
        fprintf(stderr, "ERROR - nfile : CANNOT CLOSE DESTINATION FILE. - err: %d\n", err);
        return err;
    }
        
    return 0;
}

static void _write(struct _cmd *c){
	char* filename = c->arguments[0];	
	int err = 0;
	if(filename == NULL){
		fprintf(stderr, "WARNING - %s:%d - WRITE: PLEASE PROVIDE A VALIDE FILENAME, NULL GIVEN.\n", __FILE__, __LINE__);
		return;
	}
	
	//si le chemin existe deja on ajoute au fichier existant
	unsigned int currentInumber;
	if( (currentInumber = inumber_of_path(currentDir)) < 1){
		fprintf(stderr, "ERROR - %s:%d - WRITE : CANNOT DETERIMNE CURRENT DIRECTORY INUMBER.\n", __FILE__, __LINE__);
		return;
	}
	if((currentInumber = inumber_of_basename(currentInumber, filename) > 0)){
		fprintf(stderr, "WARNING - %s:%d - WRITE : FILE %s ALREADY EXISTS: REWRITING FILE.\n", __FILE__, __LINE__, filename);
		rm(filename);
	}
	if(c->arguments[1] == NULL )
		err = write(filename);
	else
		err = append(filename, c->arguments );
	printf("write returned : %d\n", err);
}

static char cat(const char* filename){
	assert(filename != NULL);

	char err = 0;
	char* fullpath = expandPath(filename);
	unsigned int inumber = inumber_of_path(fullpath);
	if(inumber < 1){
		_printf_err("CAT : NOT SUCH FILE OR DIRECTORY. fullpath : %s\n", fullpath);
		return -27;
	}
	file_desc_str fd;
	if( (err = open_ifile(&fd, inumber)) ){
		_print_err("CAT : FILE WAS FOUND BUT COULD NOT BE OPENED.\n");
		return -28;
	}
	int car;
	while( (car = readc_ifile(&fd)) != READ_EOF){
		if(car < 0){
			_printf_err("CAT : CANNOT GET NEXT CHARACTER. err=%d\n", car);
			continue;
		}
		printf("%c", car);
	}
	if( (err = close_ifile(&fd)))
		_print_err("CAT : CANNOT CLOSE THE FILE PROPERLY.\n");
	putchar('\n');
	return err;
}

static void _cat(struct _cmd *c){
	char* filename = c->arguments[0];
	if(filename == NULL){
		_print_warn("CAT: PLEASE PROVIDE A VALIDE FILENAME, NULL GIVEN.\n");
		return;
	}
    printf("cat returned : %d\n", cat(filename));
}

static void _append(struct _cmd *c){
	char* filename = c->arguments[0];
    if(filename == NULL){
        _print_warn("APPEND: PLEASE PROVIDE A VALIDE FILENAME, NULL GIVEN.\n");
        return;
    }
	append(filename, c->arguments);
    printf("\n");
}

static void _dumps(struct _cmd *c){
	char* blockIdStr = c->arguments[0];
	int blockId = atoi(blockIdStr);
	unsigned char* buff = (unsigned char*)malloc(secSize);
	read_bloc(currentVol, blockId, buff);
	while(*buff)
		putchar(*buff++);

    printf("\n");
}

static void ping(struct _cmd *c){
	while(1)
		printf("ping ");
}
static void pong(struct _cmd *c){
	while(1)
		printf("pong ");
}

static void compute(struct _cmd *c){
	double d = 0;
	while(1){
		d += 10000;
		d  = sqrt(d);
		d *= 3;
	}
}

static void top(struct _cmd *c){
	sched_running = false;
	
	ctx_s* cur = ctx_current;
	char* statuts[] = {"READY", "ACTIVABLE", "TERMINATED", "BLOCKED"};
	printf("\n Liste des processus en cours : \n");
	printf(" PID |               NOM      |       CPU TIME     |    STATUS   |  ACTIVE \n");
	do{
		printf("%3u   %20s          %13llu   %11s     %5s\n", cur->pid, cur->name, cur->timing.cpu_time, statuts[cur->status], (cur == ctx_current) ? "ACTIVE" : "-");
	}while( (cur = cur->next) != ctx_current );
	printf("\n [Ctrl + D] pour continuer...\n");
	
getchar();
	
	sched_running = true;
}

static void _kill(struct _cmd *c){
	unsigned int pid = atoi(c->arguments[0]);
	ctx_kill(pid);
}

static void mkfs(struct _cmd *c){
	unsigned int volId = atoi(c->arguments[0]);
	makefs(volId);
}


/* ------------------------------
   dialog and execute 
   ------------------------------------------------------------*/

/* On découpe l'instruction en tableau de chaines de caractères
 * Retour : tableau de chaînes de caractères
 *	 - première chaîne = nom de la commande à executer
 *   - autres chaînes = arguments
 *   - dernier pointeur = NULL (comme pour les chaines de caractères pour indiquer qu'il n'y a rien après...)
 */
char** chopInstruction(const char* instruction){
	// on compte les paramères présents dans l'instruction 
	unsigned int argCount = 0;

	bool lastCharWasAlphanum = false;
	for(const char* cur=instruction;*cur!='\0' && *cur!='&';cur++){
		if(!isspace(*cur)){
			if(!lastCharWasAlphanum)
				argCount++;
			lastCharWasAlphanum = true;
		}else
			lastCharWasAlphanum = false;
	}
	
	char** params = (char**)calloc((1+argCount+1), sizeof(char*));
	params[argCount+1] = NULL; 		// indique qu'il n'y a plus rien à lire après.
	const char* pt = instruction;   // curseur pour parcourir l'instruction.

	unsigned int i, j;
	for(i = 0;i<argCount;i++,j=0){
		j = 0;
		params[i] = (char*)malloc(ARG_MAX_SIZE);
		while(isspace(*pt) && *pt != '&' && *pt != '\0')
			pt++;
		while(!isspace(*pt) && *pt != '&' && *pt != '\0')
			params[i][j++] = *pt++;
		params[i][j] = '\0';
	}
	return params;
}

void free_args(void* ptr){
	char** args = (char**)ptr;
	while(*args)
		free(*args++);
	free(ptr);
}

static void execute(const char *instruction){
    struct _cmd *c = commands;
	bool async = false;
	size_t len = strlen(instruction);
	if(len == 0)
		return;
	if( instruction[strlen(instruction)-1] == '&')
		async = true;
	char** args = chopInstruction(instruction); // retourne |name|arg1|arg2|...|NULL|
	while (c->name && strcmp (args[0], c->name))
		c++;
	c->arguments = &(args[1]); // on ne donne que les arguments, pas le nom de la commande.
	if(async == false){
		(*c->fun)(c);
		char** cur = args;
		while(*cur != NULL)
			free(*cur++);
		free(args);
	}else{
		ctx_s *ctx = create_ctx(6553500, (func_t)c->fun, c);
		ctx_set_name(ctx, args[0]);
		ctx_set_on_terminate(ctx, free_args, args);
	}
}

static void loop(FILE* stream){
    char *instruction = (char*)malloc(200);

	if( stream == stdin){
		char prompt[200];
		sprintf(prompt, "%s> ", currentDir);
		while( (instruction = readline(prompt)) ){
			execute(instruction);
		}	
	}else{	
		while (printf("%s> ", currentDir), fgets(instruction, 200, stream) == instruction){
			if(stream != stdin)
				printf("%s", instruction);
			instruction[strlen(instruction)-1] = '\0';
			execute(instruction);
		}
	}
	xit(NULL);
}

/* ------------------------------
   command execution 
   ------------------------------------------------------------*/

static void do_xit(){
    printf("Terminating all contexts (currently %u) \n", sched_get_ctx_count());
	sched_terminate_all();
}

static void xit(struct _cmd *dummy){
    do_xit(); 
}

static void help(struct _cmd *dummy){
    struct _cmd *c = commands; 
    for (; c->name; c++) 
		printf ("--------------------\n%s\n %s\n", c->name, c->comment);
}

static void none(struct _cmd *c){
    printf ("%s\n", c->comment) ;
}

void bsh_start(void* args){
    char** argv = (char**)args;
	sprintf(currentDir, "/");
	cd("/");

	FILE* stream = stdin;
	if(argv && !(stream = fopen(argv[1], "r")))
		stream = stdin;
	loop(stream);

    return;
}

/*
 * initialisation du shell:
 *    > montage du disque
 *    > init répertoire courrant (toujours /)
 *    > ouverture de l'inode et stockage dans currentInode
 *    > prompt
 */

int main(int argc, char **argv){
	printf("\n    Shell de base pour la gestion de fichiers - ASE++\n    =================================================\n               Alexis Beaujet M1\n\n\n");
    boot(bsh_start, (argc > 1) ? argv : NULL);
    // on n'ira jamais plus loin
	return 0;
}
