#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "hardware.h"

#include "drive.h"
#include "sched.h"
#include "disk.h"
#include "vol.h"
#include "mbr.h"

extern unsigned int secSize;
extern mbr_str MBR;

extern char disk_running; // nous vient de très loin: disk.c et nous indique si on doit utiliser sleep(HDA_IRQ) ou si on considère qu'on utilise la FSM sur interruption de disk.c 

int checkMBR(){
	return (MBR.magic == MBR_MAGIC) ? 0 : -1;
}

void mbr_load(){
	unsigned char* tmpbuffer = (unsigned char*)calloc(secSize, sizeof(unsigned char));
	if(disk_running)
		disk_read(0, 0, tmpbuffer);
	else
		read_sector(0, 0, tmpbuffer);
	memcpy(&MBR, tmpbuffer, sizeof(mbr_str));
	free(tmpbuffer);
}

void mbr_write(){
	unsigned char* tmpbuffer = (unsigned char*)calloc(secSize, sizeof(unsigned char));
	memcpy(tmpbuffer, &MBR, sizeof(mbr_str));
	if(disk_running)
		disk_write(0, 0, tmpbuffer);
	else
		write_sector(0, 0, tmpbuffer);
	free(tmpbuffer);
}

void mbr_print(){ // MBR déjà chargé dans la structure.
	if(MBR.magic != MBR_MAGIC){
		printf("MBR invalide ou non initialise. Impossible de lire la table des volumes.\n\n");
		return;
	}
	printf(" MBR : %d volumes enregistres.\n", MBR.volCount);
	for(int i = 0;i<MBR.volCount;i++){
		printf("\tVol %d/%d :\n", i, MBR.volCount);
		printf("\t\tstart: \t(%x,%x)\n", MBR.volumes[i].cyl, MBR.volumes[i].sec);
		printf("\t\tsize: \t0x%04x\n", MBR.volumes[i].size);
		printf("\t\ttype: \r%u\n", MBR.volumes[i].type);
	}
}


