#ifndef DISK_H_INCLUDED
#define DISK_H_INCLUDED

#define DISK_MODE_NONE		0
#define DISK_MODE_SEMAPHORE 1
#define DISK_MODE_SCHEDULED 2


extern char disk_running;

void disk_init(char disk_mode);
void disk_write(unsigned int cyl, unsigned int sect, unsigned char* buffer);
void disk_read(unsigned int cyl, unsigned int sect, unsigned char* buffer);
void disk_format(unsigned int cyl, unsigned int sect);
#endif
