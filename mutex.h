#ifndef MUTEX_H
#define MUTEX_H

typedef struct mutx_s{
	Semaphore 	sem;
	ctx_s*		owner;
} Mutex;

void mtx_init(Mutex *);
void mtx_lock(Mutex *);
void mtx_unlock(Mutex *);

#endif
