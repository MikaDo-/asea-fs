#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "hardware.h"
#include "drive.h"
#include "vol.h"
#include "mbr.h"
#include "inode.h"
#include "ifile.h"
#include "mount.h"

int pfile(unsigned int inumber){
    file_desc_str fd;
    int err = 0;
    int c;
    
    if( (err = open_ifile(&fd, inumber)) ){
		fprintf(stderr, "ERROR - pfile : CANNOT OPEN THE FILE. - err: %d\n", err);
        return err;
    }

    while((c=readc_ifile(&fd)) != READ_EOF)
        putchar(c);
	printf("\n");

    if( (err = close_ifile(&fd)) ){
		fprintf(stderr, "ERROR - pfile : ERROR WHILE CLOSING THE FILE. - err: %d\n", err);
        return err;
    }
    return err;
}

void usage(const char *prgm){
    fprintf(stderr, "[%s] usage:\n\t"
            "%s inumber\n", prgm, prgm);
    exit(EXIT_FAILURE);
}

int main (int argc, char *argv[]){
    unsigned inumber;
    
    if (argc != 2)
        usage(argv[0]);

    errno = 0;
    inumber = strtol(argv[1], NULL, 10);
    if (errno || inumber <= 0)
        usage(argv[0]);

    initConfig();
    mount();
    pfile(inumber);
    umount();
    
    exit(EXIT_SUCCESS);         
}
