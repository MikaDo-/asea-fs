#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hardware.h"
#include "drive.h"
#include "vol.h"
#include "mbr.h"
#include "inode.h"
#include "ifile.h"
#include "mount.h"
#include "dir.h"

int nfile(){
    file_desc_str fd;
    unsigned int inumber;
    int err = 0;
    int c;
    
    if( (err = create_ifile(FT_FILE)) <= 0 ){
		fprintf(stderr, "ERROR - nfile : CANNOT CREATE DESTINATION FILE. - err: %d\n", err);
        return err;
    }
    inumber = (unsigned int)err;
    printf("Inumber du fichier cree : %u\n", inumber);

    if( (err = open_ifile(&fd, inumber)) ){
		fprintf(stderr, "ERROR - nfile : CANNOT OPEN DESTINATION FILE. - err: %d\n", err);
        return err;
    }
	char filename[20] = {'\0'};
	sprintf(filename, "file-%u.txt", inumber);
	err = add_entry(1, inumber, filename);
	printf("Ajout de l'entree dans le repertoire racine : %d\n", err);
    
    while((c=getchar()) != EOF)
        if( (err = writec_ifile(&fd, c)) < 0 ){
            fprintf(stderr, "ERROR - nfile : CANNOT WRITE INTO DESTINATION FILE. - err: %d\n", err);
            return err;
        }

    if( (err = close_ifile(&fd)) ){
        fprintf(stderr, "ERROR - nfile : CANNOT CLOSE DESTINATION FILE. - err: %d\n", err);
        return err;
    }
        
    return 0;
}

static void usage(const char *prgm){
    fprintf(stderr, "[%s] usage:\n\t"
            "%s\n", prgm, prgm);
    exit(EXIT_FAILURE);
}

int main (int argc, char *argv[]){
    if (argc != 1)
        usage(argv[0]);

    initConfig();
    mount();
    //nfile();
    boot(nfile, NULL);
    umount();
    
    exit(EXIT_SUCCESS);         
}
