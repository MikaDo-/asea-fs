#ifndef VOL_H_INCLUDED
#define VOL_H_INCLUDED

#define VOL_INFO_SIZE	sizeof(vol_str)
#define SUPERBLOCK_MAGIC 0xEF53
#define CHECK_SUPERBLOCK_LOADED	        if(currentSuperblock.magic != SUPERBLOCK_MAGIC){ printf("ERREUR : SUPERBLOCK NON CHARGE !"); return -1; }
#define CHECK_SUPERBLOCK_LOADED_CHAR	if(currentSuperblock.magic != SUPERBLOCK_MAGIC) return "ERREUR : SUPERBLOCK NON CHARGE !";

typedef struct superblock_str{
	unsigned int 	magic;
	unsigned int 	serial;
	char 			volName[32];
	unsigned int 	firstInode;
	unsigned int 	firstFree;
} superblock_str;

typedef struct vol_str{
	unsigned int cyl;
	unsigned int sec;
	unsigned int size; // en blocs
	unsigned char type; // 1 : fs  /  2 : annexe  /  2 : non-fs
} vol_str;

typedef struct free_list_block{
	unsigned int prev;
	unsigned int next;
	unsigned int blockCount;
	unsigned int pos;
} free_list_block;

int checkVolAddr(unsigned int vol, unsigned int bloc);
int blockToAddr(unsigned int vol, unsigned int bloc, unsigned int *cyl, unsigned int *sect);
int read_bloc(unsigned int vol, unsigned int nbloc, unsigned char *buffer);
int write_bloc(unsigned int vol, unsigned int nbloc, unsigned char *buffer);
int format_vol(unsigned int vol);


void init_super(unsigned int vol);
int load_superblock(int vol);
int save_superblock();
int init_root();
int load_freeblock(unsigned int pos, free_list_block* blockInfo);
int write_freeblock(unsigned int pos, free_list_block* blockInfo);
int new_block();
int free_block(unsigned int blockId);
int makefs(unsigned int volId);
void dump_superblock();
int isVolFull();
int volFreeSpace();
char* vol_name();
int disp_freelist();

#endif
