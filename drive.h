#ifndef DRIVE_H_INCLUDED
#define DRIVE_H_INCLUDED

#define ENABLE_HDA      1
#define HDA_CMDREG      0x3F6
#define HDA_DATAREGS    0x110
#define HDA_IRQ         14

void mFormat(unsigned int cyl, unsigned int sect, unsigned int nbSec, unsigned int val);
void format_sector(unsigned int cylinder, unsigned int sector, unsigned int nSector);
void mSeek(unsigned int cyl, unsigned int sec);
void initDisk();
void getGeometry(unsigned int *nbCyl, unsigned int *nbSec, unsigned int* tailleSec);
void read_sector(unsigned int cylinder, unsigned int sector, unsigned char* buffer);
void write_sector(unsigned int cylinder, unsigned int sector, const unsigned char* buffer);
void makeSampleDisk();

#endif
