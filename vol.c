#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "signal.h"

#include "hardware.h"
#include "debug_tools.h"
#include "drive.h"
#include "sched.h"
#include "disk.h"
#include "vol.h"
#include "mbr.h"
#include "inode.h"
#include "ifile.h"

#define  CHECK_MBR_AND_ADDRESS		if(checkMBR() < 0) return -2; int errtmp; if( (errtmp =checkVolAddr(vol, nbloc)) < 0) return errtmp;

extern mbr_str MBR;
extern unsigned int secSize;
extern unsigned int secsPerCyl;

superblock_str currentSuperblock;
unsigned int currentVol;
unsigned char buffer[512];
extern char disk_running; // nous vient de très loin: disk.c et nous indique si on doit utiliser sleep(HDA_IRQ) ou si on considère qu'on utilise la FSM sur interruption de disk.c 

int checkVolAddr(unsigned int vol, unsigned int bloc){
	if(vol >= MBR.volCount || bloc >= MBR.volumes[vol].size)
		return -3;
	return 0;
}

int blockToAddr(unsigned int vol, unsigned int nbloc, unsigned int *cyl, unsigned int *sect){
	CHECK_MBR_AND_ADDRESS
	
    unsigned int sec; // on remet tout en secteurs.
    sec  = MBR.volumes[vol].cyl * secsPerCyl;
    sec += MBR.volumes[vol].sec;
    sec += nbloc;
    
    *cyl = sec / secsPerCyl;
	*sect = sec % secsPerCyl;
    
	return 0;
}

int read_bloc(unsigned int vol, unsigned int nbloc, unsigned char *buffer){
	CHECK_MBR_AND_ADDRESS
	unsigned int cyl, sect;
	blockToAddr(vol, nbloc, &cyl, &sect);
	if(disk_running)
		disk_read(cyl, sect, buffer);
	else
		read_sector(cyl, sect, buffer);
	return 0;
}

int write_bloc(unsigned int vol, unsigned int nbloc, unsigned char *buffer){
	CHECK_MBR_AND_ADDRESS
	unsigned int cyl, sect;
	blockToAddr(vol, nbloc, &cyl, &sect);
	if(disk_running)
		disk_write(cyl, sect, buffer);
	else
		write_sector(cyl, sect, buffer);
	return 0;
}

int format_vol(unsigned int vol){
	if(checkMBR() < 0)
		return -2;
	if(vol >= MBR.volCount)
		return -1;
	format_sector(MBR.volumes[vol].cyl, MBR.volumes[vol].sec, MBR.volumes[vol].size);
	_dbreak("FORMAT TERMINE\n");
	return 0;
}

void init_super(unsigned int vol){
	if(vol > 8)
		return;
	superblock_str sblock;
	sblock.magic = SUPERBLOCK_MAGIC;
	sblock.serial = 0xDEADBEEF;
	sprintf(sblock.volName, "VOL_%u", vol);
	sblock.firstInode = 1;
	sblock.firstFree = 0;

	memcpy(buffer, &sblock, sizeof(superblock_str));
    write_bloc(vol, 0, buffer);
}

int load_superblock(int vol){
	currentVol = vol;
	int err = read_bloc(vol, 0, buffer);
    if(err)
        return err;
    memcpy(&currentSuperblock, buffer, sizeof(superblock_str));
    return (currentSuperblock.magic == SUPERBLOCK_MAGIC) ? 0 : -18;
}

int save_superblock(){
	CHECK_SUPERBLOCK_LOADED
	memcpy(buffer, &currentSuperblock, sizeof(superblock_str));
    return write_bloc(currentVol, 0, buffer);
}

// crée le répertoire racine pour le fs
int init_root(){
	CHECK_SUPERBLOCK_LOADED
	return create_ifile(FT_DIRECTORY);
}

int load_freeblock(unsigned int pos, free_list_block* blockInfo){
    if(pos == 0)
        return -6;
    int err;
    if( (err = read_bloc(currentVol, pos, buffer)) )
        return err;
	memcpy(blockInfo, buffer, sizeof(free_list_block));
    return 0;
}

int write_freeblock(unsigned int pos, free_list_block* blockInfo){
	memcpy(buffer, blockInfo, sizeof(free_list_block));
	return write_bloc(currentVol, pos, buffer);
}

void dump_superblock(){
    printf("INFO  - Current superblock content :\n");
    printf("INFO  -    volname   : %s\n", currentSuperblock.volName);
    printf("INFO  -    magic     : 0x%08x\n", currentSuperblock.magic);
    printf("INFO  -    serial    : 0x%08x\n", currentSuperblock.serial);
    printf("INFO  -    firstFree : %u\n", currentSuperblock.firstFree);
    printf("INFO  -    firstNode : %u\n", currentSuperblock.firstInode);
}

int new_block(){
    int err = 0;
	CHECK_SUPERBLOCK_LOADED
    free_list_block firstFree, nextFree;
    if( (err = load_freeblock(currentSuperblock.firstFree, &firstFree)) )
        return err;
    if(firstFree.blockCount > 1){ // on crée un nouveau bloc libre (taille-1, pos+1, suiv=, prev=)
        nextFree = firstFree; // on crée les informations du nouveau bloc libre à partir de l'actuel
        nextFree.blockCount--;
        nextFree.pos++;
        nextFree.next = firstFree.next;
        currentSuperblock.firstFree = nextFree.pos;
        if( nextFree.pos != 0 && (err = write_freeblock(nextFree.pos, &nextFree)) )
            return err;
    }else{
        if(firstFree.next > 0){
            load_freeblock(firstFree.next, &nextFree);
            nextFree.prev = 0;
        }
        currentSuperblock.firstFree = firstFree.next;
        if( firstFree.next != 0 && (err = write_freeblock(nextFree.pos, &nextFree)) )
            return err;
    }
    // on sauvegarde le nouveau superblock (premier block libre a changé) et le premier "bloc de blocs" libre
    if( (err = save_superblock()) )
        return err;
    return firstFree.pos; // on retourne la position du bloc alloué.
}

int free_block(unsigned int blockId){ // n'est pas efficace et fragmente le système de fichiers ! Il vaut mieux libérer les secteurs par segments
	CHECK_SUPERBLOCK_LOADED
    int err = 0;
    // deux cas :
    //    -> le disque est plein et le bloc libéré sera le premier de la freelist
    //    -> le disque n'est pas plein et le bloc libéré est à mettre en tête de liste.
    
    free_list_block firstFree; // firstFree car le bloc libéré sera mis en tête de liste chaînée.
    firstFree.pos = blockId;
    firstFree.blockCount = 1;
    firstFree.prev = 0;
    firstFree.next = currentSuperblock.firstFree;
    
    if(currentSuperblock.firstFree != 0){ // si le disque n'est pas vide, il faut mettre à jour le prev de l'ancien first
        free_list_block currentFirstFree;
        if( (err = load_freeblock(currentSuperblock.firstFree, &currentFirstFree)) )
            return err;
        currentFirstFree.prev = firstFree.pos;
        if( (err = write_freeblock(currentSuperblock.firstFree, &currentFirstFree)) )
            return err;
    }
    currentSuperblock.firstFree = firstFree.pos;
    if( (err = write_freeblock(firstFree.pos, &firstFree)) )
        return err;
    err = save_superblock();
    
    return err;
}

int isVolFull(){
	CHECK_SUPERBLOCK_LOADED
    return currentSuperblock.firstFree == 0;
}

char* vol_name(){
	CHECK_SUPERBLOCK_LOADED_CHAR
    return currentSuperblock.volName;
}

// en nombre de blocs
int volFreeSpace(){
    if(currentSuperblock.firstFree == 0)
        return 0;
    unsigned int blockCount = 0;
    free_list_block freeBlock;
    load_freeblock(currentSuperblock.firstFree, &freeBlock);
    do{
        blockCount += freeBlock.blockCount;
    }while(freeBlock.next && load_freeblock(freeBlock.next, &freeBlock) == 0);
    return blockCount;
}

int disp_freelist(){
    if(currentSuperblock.firstFree == 0)
        return 0;
    free_list_block freeBlock;
    int err;
    if( (err = load_freeblock(currentSuperblock.firstFree, &freeBlock)) )
        return err;
    printf("\n\tDUMP FREELIST :\n");
    do{
        printf("> freeBlock.pos : %u\n", freeBlock.pos);
        printf("> freeBlock.prev: %u\n", freeBlock.prev);
        printf("> freeBlock.next: %u\n", freeBlock.next);
        printf("> freeBlock.size: %u\n\n", freeBlock.blockCount);
    }while(freeBlock.next && load_freeblock(freeBlock.next, &freeBlock) == 0);
    return 0;
}

// pour créer système de fichiers, créer superblock et permier élélment de freelist.
int makefs(unsigned int volId){
    int err = 0;
    // init superblock
    init_super(volId);
    load_superblock(volId);
    
    // init freelist
    free_list_block firstFree;
    firstFree.pos = 1;
    firstFree.blockCount = MBR.volumes[volId].size-1;
    firstFree.prev = 0;
    firstFree.next = 0;
    
    if( (err = write_freeblock(firstFree.pos, &firstFree)) )
        return err;
    
    
    // on dit au superblock que le premier segment libre se trouve au bloc 1
    currentSuperblock.firstFree = 1;
    if( (err = save_superblock()) )
        return err;

	init_root();
    
    return 0;
}

#warning WIP: Finir défrag freelist
/*
int defragFreelist(){
	int err = 0;
	unsigned int totalSecs;
	unsigned int *blocks;
	// c'est au MBR qu'on demande la taille en blocks de la partition.
	// dans ce tableau, 0: occupé, 1:libre
	blocks = (unsigned int*)calloc(totalBlocks, sizeof(unsigned int));
	
	// on parcourt la freelist en ajoutant TOUS les blocs libres dans le tableau déclaré au dessus.
    free_list_block freeBlock;
    if( (err = load_freeblock(currentSuperblock.firstFree, &freeBlock)) )
        return err;
    printf("DEBUG - Parcours de la freelist afin de lister les blocks libres :\n");
    do{
		for(unsigned int i = 0;i<freeBlock.blockCount;i++)
			blocks[freeBlock.pos+i] = 1;
    }while(freeBlock.next && load_freeblock(freeBlock.next, &freeBlock) == 0);
    // Ensuite on additionne tous les 1 contigüs dans les première cases de chaque segment de 1.

	for(unsigned int i = 0;i<totalSecs;i++){
	}
	return err;
}*/
