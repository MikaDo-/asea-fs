#include <stdlib.h>
#include "hardware.h"
#include "timer.h"
#include "sched.h"
#include "semaphore.h"

extern ctx_s* ctx_current;

void sem_init(Semaphore *s, int N){
	s->queue = NULL;
	s->queue_end = NULL;
	s->counter = N;
	s->maxCounter = N;
}

void sem_up(Semaphore* sem){
	irq_disable();
	sem->counter++;
	sem->counter = (sem->counter >= sem->maxCounter) ? sem->maxCounter : sem->counter; // on ne veut pas dépasser maxCounter;
	if(sem->counter <= 0){ // on libère un contexte de la liste.
		ctx_s* tmp = sem->queue;
		sem->queue = sem->queue->semListNext;
		if(sem->counter == 0)
			sem->queue_end = NULL;
		tmp->semListNext = NULL;
		tmp->status = CTXS_ACTIVABLE;
	}
	irq_enable();
}

void sem_down(Semaphore* sem){
	irq_disable();
	sem->counter--;
	if(sem->counter < 0){ // bloquer tâche, on la met en bout de queue
		ctx_current->semListNext = sem->queue_end;
		sem->queue_end = ctx_current;
		sem->queue_end->status = CTXS_BLOCKED;
		if(sem->counter == -1)
			sem->queue = sem->queue_end;
		yield();
		return;
	}
	irq_enable();
}


