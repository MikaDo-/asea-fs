#ifndef IFILE_H_INCLUDED
#define IFILE_H_INCLUDED

#define READ_EOF		-1

typedef struct file_desc_str {
    unsigned int    inumber;
    unsigned int    size;       // en octets
    unsigned int    pos;        // position du curseur
    unsigned char   buffer[512];
    char            needsFlush; // Il faut écrire le tampon sur le disque
} file_desc_str;

unsigned int block_of_byte(unsigned int byte);
unsigned int byte_in_block(unsigned int byte);
unsigned int create_ifile(enum file_type_e type); 
int delete_ifile(unsigned int inumber);
int open_ifile(file_desc_str *descriptor, unsigned int inumber);
int close_ifile(file_desc_str *descriptor);
int flush_ifile(file_desc_str *descriptor);
int seek_ifile(file_desc_str *descriptor, int offset);  // relatid
int seek2_ifile(file_desc_str *descriptor, int offset); // asbolu
int readc_ifile(file_desc_str *descriptor);
int writec_ifile(file_desc_str *descriptor, char c);
int read_ifile(file_desc_str *descriptor, void *buf, unsigned int nbyte);
int write_ifile(file_desc_str *descriptor, const void *buf, unsigned int nbyte);

#endif
