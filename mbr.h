#ifndef MBR_H_INCLUDED
#define MBR_H_INCLUDED

#define 	MBR_MAGIC		    0x55
#define     CHECK_MBR_OR_DIE	if(checkMBR() < 0){ printf("La table des partitions est illisible ou non initialisee. Il est necessaire d'en recreer une nouvelle.\n"); return; }
#define     CHECK_MBR_OR_DIE_I	if(checkMBR() < 0){ printf("La table des partitions est illisible ou non initialisee. Il est necessaire d'en recreer une nouvelle.\n"); return -13; }

typedef struct mbr_str{
	unsigned char volCount;
	vol_str volumes[8];
	unsigned char magic; // nous indique si la structure a un contenu valide ou pas
} mbr_str;

void mbr_load();
void mbr_write();
void mbr_print();
int checkMBR();

#endif
