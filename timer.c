#include <stdio.h>
#include <assert.h>
#include <signal.h>

#include "debug_tools.h"
#include "hardware.h"
#include "sched.h"
#include "timer.h"

extern char sched_running;

void timer_setup(){
	timer_irq_handler();
	irq_enable();
}

void timer_irq_handler(){
//_dprint("TIMER IRQ\n");
	_out(TIMER_PARAM, 3<<6);
	_out(TIMER_ALARM, 0xffffffff-20);
	if(sched_running)
		yield();
}

void irq_enable(){
	_out(TIMER_PARAM, 3<<6);
	_out(TIMER_ALARM, 0xffffffff-20);
	_mask(0);
}

void irq_disable(){
	_out(TIMER_PARAM, 2<<6);
	_mask(16); // CIMER
}


