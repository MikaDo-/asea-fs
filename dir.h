/* ------------------------------
   $Id: dir.h 7085 2013-10-18 15:37:14Z marquet $
   ------------------------------------------------------------

   Directory manipulation. 
   Philippe Marquet, november 2002
   
*/

#ifndef _DIR_H_
#define _DIR_H_

#define BASENAME_MAX_SIZE	16

int display_entries(unsigned int inumber); // inumber du répertoire à explorer.
/* return the inumber of an absolute pathname. 
   0 if not a valid pathname */
unsigned int inumber_of_path(const char *pathname);

/* pathname is an absolute pathname.
   return the inumber of the pathname dirname and
   return in basename the last entry of the pathname (basename will be
   a pointer to a char in pathname).
   return 0 if the directory does not exist. */
unsigned int dinumber_of_path(const char *pathname, const char **basename);

/* consider the directory of inumber idir. 
   search after an entry of name basename (which can not contain /). 
   return the inumber of the entry, 0 if no such entry or if idir is
   not a directory.
*/
unsigned int inumber_of_basename(unsigned int idir, const char *basename);

int add_entry(unsigned int idir, unsigned int inumber, const char *basename);
int del_entry(unsigned int idir, const char *basename);

typedef struct entry_s {
    unsigned int inumber;
    char basename[BASENAME_MAX_SIZE]; 
} entry_str; 

typedef struct fileinfo_str{
	unsigned int 	inumber;
	char* 			basename;
	file_type_e 	type;
	unsigned int 	size;
} fileinfo_str;

int testDossiers();
unsigned int create_directory();
unsigned int new_entry(file_desc_str *dir_fd);
int find_entry(file_desc_str *dir_fd, const char *basename);
int display_entries(unsigned int inumber);
unsigned int inumber_of_basename(unsigned int idir, const char *basename);
unsigned int inumber_of_path(const char *pathname);
unsigned int dinumber_of_path(const char *pathname, const char **basename);
int del_entry(unsigned int idir, const char *basename);
int getDirContents(unsigned int idir, fileinfo_str** infos, unsigned int *entryCount);

#endif
