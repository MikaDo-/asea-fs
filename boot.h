#ifndef _BOOT_H_
#define _BOOT_H_

void empty_it();
void boot(func_t fun, void* args);

#endif
