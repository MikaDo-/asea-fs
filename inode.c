#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "signal.h"
#include "hardware.h"

#include "drive.h"
#include "vol.h"
#include "mbr.h"
#include "inode.h"
#include "debug_tools.h"

extern mbr_str MBR;
extern unsigned int secSize;
extern superblock_str currentSuperblock;
extern unsigned int currentVol;
extern unsigned char buffer[512];

inode_str testInode;
indx_table_str testInd1, testInd2, testInd21;

int initTestIndx(){
    int err = 0, i, j;
    // l'inode de test est l'inode racine : placé juste après le superblock (inumber = 1)
    for (i = 0; i < 12; i++)
        testInode.direct_blocks[i] = 2+i;
    testInode.ind1_block = 14;
    testInode.ind2_block = 15;
    testInode.size = (12+INDx_CAPACITY+13*INDx_CAPACITY)*256+10; // on remplit : les blocs directs (12), les blocs indirects(20), la moitié des blocks doublement indirects (10) plus 3 blocks et 10 octets (donc encore un bloc) => total : 10+20+13+1=44.
    if( (err = write_inode(new_block(), &testInode)) )
        return err;
    // on alloue 12 data blocks directs
    for (i = 0; i < 12; i++)
        new_block();
    // ensuite, on renseigne les tables d'indirection
    // la première se trouve en inumber = 14
    // la seconde se trouve en inumber = 15
    for (i = 0; i < INDx_CAPACITY; i++){
        testInd1.inumbers[i] = 16 + i;
        testInd2.inumbers[i] = 16 + INDx_CAPACITY + i;
    }
    if( (err = write_indx_table(new_block(), &testInd1)) ) // bloc 14
        return err;
    if( (err = write_indx_table(new_block(), &testInd2)) ) // bloc 15
        return err;
    // ensuite on alloue INDx_CAPACITY data blocks
    for (i = 0; i < INDx_CAPACITY; i++)
        new_block();
        
    // on renseign les IND1 référencées par l'IND2
    for (i = 0; i < INDx_CAPACITY; i++){
        for (j = 0; j < INDx_CAPACITY; j++)
            testInd21.inumbers[j] = 16 + 2*INDx_CAPACITY + i*INDx_CAPACITY + j;
        if( (err = write_indx_table(new_block(), &testInd21)) )
            return err;
    }
    for (i = 0; i < (INDx_CAPACITY*INDx_CAPACITY); i++)
        new_block();
    /* Disque final :
     * [ SB | / | 12 * direct blocks | IND1 | IND2 | INDx_CAPACITY * IND1 data blocks | INDx_CAPACITY * IND2 IND1 tables | INDx_CAPACITY * INDx_CAPACITY * IND2IND1 Data blocks ]
     */
    return err;
}

int testAllocationDataBlocks(){
    int err = 0;
    makefs(currentVol); // on remet le fs à zéro.
    
    unsigned int inumber = create_inode(FT_FILE);
    print_inode(inumber);
    inode_str inode;
    read_inode(inumber, &inode);
    
    inode.size = 1024; // 1kiB
    
    write_inode(inumber, &inode);
    
    printf("\n\n");
    unsigned int inumbers[] = {4, 10, 15, 30, 90, 70, 1};
    int inumbersCount = 7;
    for (int i = 0; i < inumbersCount; i++)
        printf("vblock_of_fblock(inumber, %2u, true) => %u\n", inumbers[i], vblock_of_fblock(inumber,  inumbers[i], true));
    printf("\n\n");
    print_inode(inumber);
    
    return err;
}

int read_inode(unsigned int inumber, inode_str *inode){
    int err = read_bloc(currentVol, inumber, buffer);
    if(err){
		_printf_err("CANNOT READ INODE %u\n", inumber);
        return err;
	}
    memcpy(inode, buffer, sizeof(inode_str));
	if(inode->type != FT_DIRECTORY && inode->type != FT_FILE)
		inode->type = FT_UNKNOWN;
	return 0;
}

int write_inode(unsigned int inumber, inode_str *inode){
    memcpy(buffer, inode, sizeof(inode_str));
    int err = write_bloc(currentVol, inumber, buffer);
	if(err)
		_printf_err("CANNOT WRITE  INODE %u\n", inumber);
	return err;
}

int create_inode(file_type_e type){
    int nvBloc = new_block();
    if(nvBloc < 0)
        return nvBloc;
    inode_str inode;
    inode.type = type;
    inode.size = 0;
    for (int i = 0; i < 12; i++)
        inode.direct_blocks[i] = 0;
    inode.ind1_block = 0;
    inode.ind2_block = 0;
    inode.ind3_block = 0;
    inode.refCount = 0;
    write_inode(nvBloc, &inode);
    return nvBloc;
}

int delete_inode(unsigned int inumber){
	inode_str inode;
    int err = 0;
	if( (err = read_inode(inumber, &inode)) )
		return err;
	for(int i = 0;i<12;i++)
		if(inode.direct_blocks[i])
			if( (err = free_block(inode.direct_blocks[i])) ){
				_printf_err("CANNOT FREE DIRECT BLOCK %u OF INODE %u\n", i, inumber);
				return err;
			}
	if(inode.ind1_block)
		if( (err = del_ind1_table(inode.ind1_block)) )
			return err;
	if(inode.ind2_block)
		if( (err = del_ind2_table(inode.ind2_block)) )
			return err;
	if(inode.ind3_block)
		if( (err = del_ind3_table(inode.ind3_block)) )
			return err;
	return free_block(inumber);
}

int print_inode(unsigned int inumber){
	inode_str inode;
    indx_table_str ind1, ind2, ind21, ind3, ind32, ind321;
    int err = 0, i, j, k;
	if( (err = read_inode(inumber, &inode)) )
		return err;
    printf("INFO  - DISPLAYING INODE %u CONTENT : \n", inumber);
    printf("     Size             : %u\n", inode.size);
    printf("     Type             : %s\n", (inode.type == FT_FILE) ? "FT_FILE" : (inode.type == FT_DIRECTORY) ? "FT_DIRECTORY" : "????");
    printf("     Direct blocks    : \n");
    for (i = 0; i < 12; i++)
        printf("                 [%2d] : %u\n", i, inode.direct_blocks[i]);
    printf("     IND1 table       : %u\n", inode.ind1_block);
    printf("     IND1 blocks      :\n");
    if(inode.ind1_block && !(err = read_indx_table(inode.ind1_block, &ind1)))
        for (i = 0; i < INDx_CAPACITY; i++)
            printf("                 [%2d] : %u\n", i, ind1.inumbers[i]);
    printf("     IND2 table       : %u\n", inode.ind2_block);
    printf("     IND2 IND1 tables : \n");
    if(inode.ind2_block && !(err = read_indx_table(inode.ind2_block, &ind2)))
        for (i = 0; i < INDx_CAPACITY; i++){
            printf("                 [%2d] : %u Contains :\n", i, ind2.inumbers[i]);
            if(ind2.inumbers[i] && !(err = read_indx_table(ind2.inumbers[i], &ind21)))
                for (j = 0; j < INDx_CAPACITY; j++)
                        printf("                           [%2d] : %u\n", j, ind21.inumbers[j]);   
        }
    printf("     IND3 table       : %u\n", inode.ind3_block);
    printf("     IND3>IND1 tables : \n");
    if(inode.ind3_block && !(err = read_indx_table(inode.ind3_block, &ind3)))
        for (i = 0; i < INDx_CAPACITY; i++){
            printf("                 [%2d] : %u Contains :\n", i, ind3.inumbers[i]);
            if(ind3.inumbers[i] && !(err = read_indx_table(ind3.inumbers[i], &ind32)))
                for (j = 0; j < INDx_CAPACITY; j++){
                    printf("                           [%2d] : %u Contains :\n", j, ind32.inumbers[j]);   
                    if(ind32.inumbers[j] && !(err = read_indx_table(ind32.inumbers[j], &ind321)))
                        for (k = 0; k < INDx_CAPACITY; k++)
                                printf("                                     [%2d] : %u\n", k, ind321.inumbers[k]);   
                }
        }
    return err;
}

int read_indx_table(unsigned int inumber, indx_table_str *ind_table){
    int err = read_bloc(currentVol, inumber, buffer);
    if(err)
        return err;
    memcpy(ind_table, buffer, sizeof(indx_table_str));
	return 0;
}

int write_indx_table(unsigned int inumber, indx_table_str *ind_table){
    memcpy(buffer, ind_table, sizeof(indx_table_str));
    int err =  write_bloc(currentVol, inumber, buffer);
	if(err)
		_print_err("CANNOT WRITE INDx TABLE\n");
	return 0;
}

int create_indx_table(){
    int nvBloc = new_block();
    if(nvBloc < 0)
        return nvBloc;
	indx_table_str indTable;
    int err = 0;
	for (int i = 0; i < INDx_CAPACITY; i++)
        indTable.inumbers[i] = 0;
    if( (err = write_indx_table(nvBloc, &indTable)) ){
        _printf_err("CANNOT WRITE INDx TABLE %u WHILE CREATING IT.\n", nvBloc);
		return err;
	}
    return nvBloc;
}

int del_ind1_table(unsigned int inumber){
	int err = 0;
	indx_table_str table;
	if( (err = read_indx_table(inumber, &table)) )
		return err;
	for(int i =0;i<12;i++)
		if(table.inumbers[i])
			if( (err = free_block(table.inumbers[i])) ){
				_printf_err("CANNOT FREE BLOCK %u WHILE FREEING IND1 TABLE %u\n", table.inumbers[i], inumber);
				return err;
			}
	if( (err = free_block(inumber)) )
		_printf_err("CANNOT FREE IND1 TABLE %u\n", inumber);
		
	return err;
}

int del_ind2_table(unsigned int inumber){
	int err = 0;
	indx_table_str table;
	if( (err = read_indx_table(inumber, &table)) )
		return err;
	for(int i =0;i<12;i++)
		if(table.inumbers[i])
			if( (err = del_ind1_table(table.inumbers[i])) ){
				_printf_err("CANNOT FREE IND1 TABLE %u (IND2.IND1s[%d]) WHILE FREEING IND2 TABLE %u\n", table.inumbers[i], inumber, i);
				return err;
			}
	if( (err = free_block(inumber)) )
		_printf_err("CANNOT FREE IND2 TABLE %u\n", inumber);
		
	return err;
}

int del_ind3_table(unsigned int inumber){
	int err = 0;
	indx_table_str table;
	if( (err = read_indx_table(inumber, &table)) )
		return err;
	for(int i =0;i<12;i++)
		if(table.inumbers[i])
			if( (err = del_ind2_table(table.inumbers[i])) ){
				_printf_err("CANNOT FREE IND2 TABLE %u (IND3.IND2s[%d]) WHILE FREEING IND3 TABLE %u\n", table.inumbers[i], inumber, i);
				return err;
			}
	if( (err = free_block(inumber)) )
		_printf_err("CANNOT FREE IND3 TABLE %u -err: %d\n", inumber, err);
		
	return err;
}

// par résolution, j'entends "trouver le bloc de donnée 'fblock' de la table d'indirection de premier niveau 'inumber'"
int resolve_ind1(unsigned int inumber, unsigned int fblock, bool do_allocate){
	if(inumber == 0)
		return 0;
	indx_table_str ind1;
    int err = 0;
	if( (err = read_indx_table(inumber, &ind1)) )
		return err;
    // si le bloc de donnée pointé par la table est alloué, ou si on ne demande pas d'allocation, on retourne directement le numéro de block (que ce soit 0 ou autre chose).
    if(ind1.inumbers[fblock] != 0 || !do_allocate)
        return ind1.inumbers[fblock];
    if( (err = new_block()) <= 0){ // sinon, on alloue un block et on met à jour la table d'indirection. la taille de fichier sera mise à jour par les fonctions de manipulation de fichiers.
		_printf_err("CANNOT ALLOCATE A NEW DATA BLOCK IN IND1 TABLE %u - err %d.\n", inumber, ind1.inumbers[fblock]);
        return err;
    }
    ind1.inumbers[fblock] = err; // le retour de new_block() une fois qu'on a testé qu'il s'agit bien d'un bloc alloué et non d'un code d'erreur. 
    if( (err = write_indx_table(inumber, &ind1)) ){
		_printf_err("CANNOT WRITE IND1 TABLE %u AFTER NEW BLOCK ALLOCATION.\n", inumber);
        return err;
    }
    return ind1.inumbers[fblock];
}

int resolve_ind2(unsigned int inumber, unsigned fblock, bool do_allocate){
	if(inumber == 0)
		return 0;
	indx_table_str ind2;
    int err = 0;
	if( (err = read_indx_table(inumber, &ind2)) )
		return err;
	// trouver la ind1 dans la ind2
	// résoudre cette ind1 en donnant l'inumber adéquat
	unsigned int ind1Id    = fblock/INDx_CAPACITY;
	if(ind2.inumbers[ind1Id] == 0){
        if(!do_allocate)
            return 0;
        // on fait l'allocation d'une table IND1, on la sauvegarde, puis on fait une résolution dans cette table.
        ind2.inumbers[ind1Id] = create_indx_table();
        if(ind2.inumbers[ind1Id] <= 0){
            _printf_err("CANNOT ALLOCATE A NEW IND1 TABLE FOR IND2 TABLE %u.\n", inumber);
            return err;
        }
        // on sauvegarde la table IND2
        if( (err = write_indx_table(inumber, &ind2)) ){
            _printf_err("CANNOT WRITE IND2 TABLE %u AFTER NEW IND1 TABLE ALLOCATION.\n", inumber);
            return err;
        }
    }
	return resolve_ind1(ind2.inumbers[ind1Id], fblock%INDx_CAPACITY, do_allocate);
}

int resolve_ind3(unsigned int inumber, unsigned fblock, bool do_allocate){
	if(inumber == 0)
		return 0;
	indx_table_str ind3;
    int err = 0;
	if( (err = read_indx_table(inumber, &ind3)) )
		return err;
	// trouver la ind2 dans la ind3
	// résoudre cette ind2 en donnant l'inumber adéquat
	unsigned int ind2Id    = fblock/(INDx_CAPACITY*INDx_CAPACITY);
	if(ind3.inumbers[ind2Id] == 0){
        if(!do_allocate)
            return 0;
        // on fait l'allocation d'une table IND1, on la sauvegarde, puis on fait une résolution dans cette table.
        ind3.inumbers[ind2Id] = create_indx_table();
        if(ind3.inumbers[ind2Id] <= 0){
            _printf_err("CANNOT ALLOCATE A NEW IND2 TABLE FOR IND3 TABLE %u.\n", inumber);
            return err;
        }
        // on sauvegarde la table IND3
        if( (err = write_indx_table(inumber, &ind3)) ){
            _printf_err("CANNOT WRITE IND3 TABLE %u AFTER NEW IND2 TABLE ALLOCATION.\n", inumber);
            return err;
        }
    }
	return resolve_ind2(ind3.inumbers[ind2Id], fblock%(INDx_CAPACITY*INDx_CAPACITY), do_allocate);
}

int vblock_of_fblock(unsigned int inumber, unsigned int fblock, bool do_allocate){
    //printf("vblock_of_fblock(%u, %u, %d)\n", inumber, fblock, do_allocate);
	inode_str inode;
    int err = 0;
	if( (err = read_inode(inumber, &inode)) )
		return err;
	if(fblock < 12){
		if(inode.direct_blocks[fblock] != 0 || !do_allocate)
            return inode.direct_blocks[fblock];
        inode.direct_blocks[fblock] = new_block();
        if(inode.direct_blocks[fblock] <= 0){
            // erreur lors de l'allocation du nouveau bloc
            _printf_err("CANNOT ALLOCATE A NEW DIRECT DATA BLOCK IN INODE %u.\n", inumber);
            return err;
        }
        if( (err = write_inode(inumber, &inode)) ){
            // erreur lors de l'écriture de l'inode
            _printf_err("CANNOT WRITE INODE %u AFTER NEW DIRECT BLOCK ALLOCATION.\n", inumber);
            return err;
        }
        return inode.direct_blocks[fblock];
    }
	else if(fblock < 12+INDx_CAPACITY){ // si le bloc peut être référencé par une table d'indirection de premier niveau...
		// cette table d'indirection a-t-elle déjà été allouée ?
        if(inode.ind1_block == 0){
            if(!do_allocate)
                return 0;
            // on crée la table IND1 et on effectue la résolution dessus.
            err = create_indx_table();
            if(err <= 0){
                _printf_err("CANNOT ALLOCATE A NEW IND1 TABLE FOR INODE %u - err %d.\n", inumber, err);
                return err;
            }
            inode.ind1_block = err;
            // on sauvgarde l'inode
            err = write_inode(inumber, &inode);
            if( (err) ){
                // erreur lors de l'écriture de l'inode
                _printf_err("CANNOT WRITE INODE %u AFTER NEW IND1 TABLE ALLOCATION.\n", inumber);
                return err;
            }
        }
        return resolve_ind1(inode.ind1_block, fblock - 12, do_allocate);
    }
	else if(fblock < (12+INDx_CAPACITY) + INDx_CAPACITY*INDx_CAPACITY){ // de second niveau...
		// cette table d'indirection a-t-elle déjà été allouée ?
        if(inode.ind2_block == 0){
            if(!do_allocate)
                return 0;
            // on crée la table IND2 et on effectue la résolution dessus.
            inode.ind2_block = create_indx_table();
            if(inode.ind2_block <= 0){
                _printf_err("CANNOT ALLOCATE A NEW IND2 TABLE FOR INODE %u.\n", inumber);
                return err;
            }
            // on sauvgarde l'inode
            if( (err = write_inode(inumber, &inode)) ){
                // erreur lors de l'écriture de l'inode
                _printf_err("CANNOT WRITE INODE %u AFTER NEW IND2 TABLE ALLOCATION.\n", inumber);
                return err;
            }
        }
		return resolve_ind2(inode.ind2_block, fblock - 12 - INDx_CAPACITY, do_allocate);
    }
	else if(fblock < ((12+INDx_CAPACITY) + INDx_CAPACITY*INDx_CAPACITY) + INDx_CAPACITY*INDx_CAPACITY*INDx_CAPACITY){ // de TROISIEME niveau...
		// cette table d'indirection a-t-elle déjà été allouée ?
        if(inode.ind3_block == 0){
            if(!do_allocate)
                return 0;
            // on crée la table IND3 et on effectue la résolution dessus.
            inode.ind3_block = create_indx_table();
            if(inode.ind3_block <= 0){
                _printf_err("CANNOT ALLOCATE A NEW IND3 TABLE FOR INODE %u.\n", inumber);
                return err;
            }
            // on sauvgarde l'inode
            if( (err = write_inode(inumber, &inode)) ){
                // erreur lors de l'écriture de l'inode
                _printf_err("CANNOT WRITE INODE %u AFTER NEW IND3 TABLE ALLOCATION.\n", inumber);
                return err;
            }
        }
		return resolve_ind3(inode.ind3_block, fblock - 12 - INDx_CAPACITY - INDx_CAPACITY*INDx_CAPACITY, do_allocate);
    }
	// si fblock est trop grand pour être contenu dans les tables d'indirection, alors on insulte l'utilisateur...
	_printf_err("FBLOCK OUT OF RANGE WHILE LOOKING FOR FBLOCK %u IN INODE %u\n", fblock, inumber);
	return -18;
}
