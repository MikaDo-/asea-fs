#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "debug_tools.h"
#include "hardware.h"
#include "timer.h"
#include "drive.h"
#include "mount.h"
#include "sched.h"
#include "disk.h"

void empty_it(){}

extern ctx_s* disk_ctx;

/*
 *    Que doit-on faire dans le boot ?
 * --------------------------------------
 *
 * Initialiser les structures de donnée modélisant le matériel
 * (la pluspart sont initialisés statiquement...)
 *   > sched
 *   > disk
 * Initialiser le matériel
 *   > init simulateur
 *   > timer
 *   > IRQVECTOR
 * Monter le disque (en conservant les appels 'bas niveau' (sans sched ni disk.c)
 * Planifier le contexte du disque sur interruption + ajouter contexte principal du programme à lancer :)
 * Démarrer l'ordonnancement : start_sched();
 */
/*
	comme je vais déplacer l'initialisation du simulateur dans ce ficiher, la compilation de certains exécultables va chialer. Il faudra ajouter boot.o à leurs dépendances.
*/

void dummy(void *nothing){
	yield();
}

void boot(func_t fun, void* args){
	int err = 0;
	initConfig();

	// load disk geometry, MBR, current volume...
	if( (err = mount()) )
		return;

	// init timer
	timer_setup();
	IRQVECTOR[TIMER_IRQ] = timer_irq_handler; // yield est maintenant appelée à chaque interruption.

	// init sched
	sched_init();

	// vu qu'il s'agit d'une liste chaînée, les contextes sont à ajouter dans l'ordre inverse de leur exécution.
	ctx_s* boot_ctx = create_ctx(65535000, fun, args);
	ctx_set_name(boot_ctx, "boot_ctx");

	// init disk FSM / ou sémaphore
	disk_init(DISK_MODE_SCHEDULED);

	// on démarre l'ordonnancement.
	start_sched();
}
