#ifndef SEMAPHORE_H
#define SEMAPHORE_H

struct ctx_s; // déclaration partielle pour définir les Sémaphores.

typedef struct sem_s{
	ctx_s*	queue;
	ctx_s*	queue_end;
	int		counter;
	int		maxCounter;
} Semaphore;

void sem_init(Semaphore *s, int N);
void sem_up(Semaphore* sem);
void sem_down(Semaphore* sem);

#endif
