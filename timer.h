#ifndef _TIMER_H_
#define _TIMER_H_

#define TIMER_IRQ	 0x02
// ports du 'PIT' :
#define TIMER_CLOCK  0xF0
#define TIMER_PARAM  0xF4
#define TIMER_ALARM  0xF8

void start_hw();

void timer_setup();
void timer_irq_handler();
void irq_enable();
void irq_disable();

#endif
